package com.paul.ttf.Models;

import com.google.gson.annotations.SerializedName;

public class SurvayRequestModel {

    @SerializedName("MemberID")
    public Integer MemberID;

    @SerializedName("CategoryID")
    public Integer CategoryID;

    @SerializedName("VideoID")
    public Integer VideoID;

    @SerializedName("PopupID")
    public Integer PopupID;

    @SerializedName("TestQueryID")
    public Integer TestQueryID;


    @SerializedName("PopValue")
    public Double PopValue;


    public Integer getMemberID() {
        return MemberID;
    }

    public void setMemberID(Integer memberID) {
        MemberID = memberID;
    }

    public Integer getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(Integer categoryID) {
        CategoryID = categoryID;
    }

    public Integer getVideoID() {
        return VideoID;
    }

    public void setVideoID(Integer videoID) {
        VideoID = videoID;
    }

    public Integer getPopupID() {
        return PopupID;
    }

    public void setPopupID(Integer popupID) {
        PopupID = popupID;
    }

    public Double getPopValue() {
        return PopValue;
    }

    public void setPopValue(Double popValue) {
        PopValue = popValue;
    }

    public Integer getTestQueryID() {
        return TestQueryID;
    }

    public void setTestQueryID(Integer testQueryID) {
        TestQueryID = testQueryID;
    }
}