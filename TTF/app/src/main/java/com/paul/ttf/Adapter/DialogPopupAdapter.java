package com.paul.ttf.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.paul.ttf.InterfaceModule.ButtonInterface;
import com.paul.ttf.Models.CategoryVideoModel;
import com.paul.ttf.R;
import com.paul.ttf.databinding.LeftItemLayoutBinding;

import java.util.ArrayList;

public class DialogPopupAdapter extends RecyclerView.Adapter<DialogPopupAdapter.MyViewHolder> implements View.OnClickListener {

    ArrayList<CategoryVideoModel.Video_List.settimepopup.ButtonList> productList;
    Context context;
    ButtonInterface onItemClick;

    public DialogPopupAdapter(ArrayList<CategoryVideoModel.Video_List.settimepopup.ButtonList> productList, Context context, ButtonInterface onItemClick) {
        this.productList = productList;
        this.context = context;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view;

        MyViewHolder viewHolder = null;
        Log.e("TAGGG", "onCreateViewHolder POSI > " + i);

        view = LayoutInflater.from(context).inflate(R.layout.alart_button_layout, parent, false);

        /*LeftItemLayoutBinding inflate = DataBindingUtil
                .inflate(LayoutInflater
                        .from(parent.getContext()), R.layout.alart_button_layout, parent, false);*/
        viewHolder = new MyViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int position) {
        final CategoryVideoModel.Video_List.settimepopup.ButtonList _object = productList.get(position);
        myViewHolder.set_category(_object);

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private static final String LOG_TAG = "Holder";
        private CategoryVideoModel.Video_List.settimepopup.ButtonList category;
        Button btn_click;

        CategoryVideoModel.Video_List.settimepopup.ButtonList _category;


        public CategoryVideoModel.Video_List.settimepopup.ButtonList getCategory() {
            return category;
        }

        public void setCategory(CategoryVideoModel.Video_List.settimepopup.ButtonList category) {
            this.category = category;
        }

        public MyViewHolder(View itemView) {
            super(itemView);


            btn_click = itemView.findViewById(R.id.btn_click);
            btn_click.setOnClickListener(DialogPopupAdapter.this);

            //  iv_product_thumb = (ImageView) itemView.findViewById(R.id.iv_product_thumb);
            // txt_category_name = (TextView) itemView.findViewById(R.id.txt_category_name);


        }

        public CategoryVideoModel.Video_List.settimepopup.ButtonList get_category() {
            return _category;
        }

        public void set_category(CategoryVideoModel.Video_List.settimepopup.ButtonList _category) {
            this._category = _category;
            btn_click.setText(_category.getButtonName());
            btn_click.setTag(_category);
            GradientDrawable shape = new GradientDrawable();
            shape.setCornerRadius(10);

            if (_category.getButtonColor() != null && !_category.getButtonColor().equals("")) {
                shape.setColor(Color.parseColor(_category.getButtonColor()));
            } else {
                shape.setColor(context.getResources().getColor(R.color.white));
            }

            //btn_click.setBackgroundDrawable(shape);
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (position % 2 == 0) {
            return 0;
        } else {
            return 1;
        }

    }

    @Override
    public void onClick(View view) {
        CategoryVideoModel.Video_List.settimepopup.ButtonList _objects = (CategoryVideoModel.Video_List.settimepopup.ButtonList) view.getTag();
        onItemClick.onSelectButtonItem(_objects);

    }
}
