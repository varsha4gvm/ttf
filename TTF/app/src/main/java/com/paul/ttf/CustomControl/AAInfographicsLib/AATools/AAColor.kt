package com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger


object AAColor {

    fun blackColor(): String {
        return "black"
    }

    fun darkGrayColor(): String {
        return "darkGray"
    }

    fun lightGrayColor(): String {
        return "lightGray"
    }

    fun whiteColor(): String {
        return "white"
    }

    fun grayColor(): String {
        return "gray"
    }

    fun redColor(): String {
        return "red"
    }

    fun greenColor(): String {
        return "green"
    }

    fun blueColor(): String {
        return "blue"
    }

    fun cyanColor(): String {
        return "cyan"
    }

    fun yellowColor(): String {
        return "yellow"
    }

    fun magentaColor(): String {
        return "magenta"
    }

    fun orangeColor(): String {
        return "orange"
    }

    fun purpleColor(): String {
        return "purple"
    }

    fun brownColor(): String {
        return "brown"
    }

    fun clearColor(): String {
        return "clear"
    }

}
