package com.paul.ttf.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.paul.ttf.CustomControl.RoundRectCornerImageView;
import com.paul.ttf.InterfaceModule.CategoryVideoInterface;
import com.paul.ttf.Models.CategoryVideoModel;
import com.paul.ttf.R;
import com.paul.ttf.UI.ApplicationClass;
import com.paul.ttf.UI.SurvayActivity;
import com.paul.ttf.Utility.FileLog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CategoryVIdeoAdapter extends RecyclerView.Adapter<CategoryVIdeoAdapter.MyViewHolder> implements View.OnClickListener {

    ArrayList<CategoryVideoModel.Video_List> productList;
    Activity context;
    CategoryVideoInterface onItemClick;
    boolean isMember;
    int Categoryid = 0;

    public CategoryVIdeoAdapter(ArrayList<CategoryVideoModel.Video_List> productList, Activity context, CategoryVideoInterface onItemClick, boolean isMember, int Categoryid) {
        FileLog.e("TAGGGG", "fillList get > " + productList.size());
        this.productList = productList;
        this.context = context;
        this.onItemClick = onItemClick;
        this.isMember = isMember;
        this.Categoryid = Categoryid;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view;
        MyViewHolder viewHolder = null;
        Log.e("TAGGG", "fillList get onCreateViewHolder POSI > " + i);

        view = LayoutInflater.from(context).inflate(R.layout.video_item_layout, parent, false);
        viewHolder = new MyViewHolder(context, view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int position) {
        final CategoryVideoModel.Video_List _object = productList.get(position);
        FileLog.e("TAGGG", "fillList get onBindViewHolder > " + _object.getVideoName());
        myViewHolder.txt_video_title.setText(_object.getVideoName());
        myViewHolder.setCategory(_object);
        myViewHolder.fab.setTag(_object);

        if (_object.getThumbnailPath() != null) {
            Glide.with(ApplicationClass.applicationContext)
                    .load(_object.getThumbnailPath())
                    .transform(new CenterCrop(context))
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.ttf_black)
                    .into(myViewHolder.iv_product_thumb);
        }

        if (_object.isPause()) {
            myViewHolder.fab.setImageDrawable(context.getResources().getDrawable(R.drawable.pause_og));
        } else {
            myViewHolder.fab.setImageDrawable(context.getResources().getDrawable(R.drawable.play_og));
        }

        if (_object.getSettimepopup().size() != 0 && isMember) {
            myViewHolder.fab_history.setVisibility(View.VISIBLE);
        } else {
            myViewHolder.fab_history.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        FileLog.e("TAGGGG", "fillList getItemCount > " + productList.size());
        return productList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private static final String LOG_TAG = "Holder";
        private CategoryVideoModel.Video_List category;
        RoundRectCornerImageView iv_product_thumb;
        FloatingActionButton fab, fab_history;
        TextView txt_video_title;


        public CategoryVideoModel.Video_List getCategory() {
            return category;
        }

        public void setCategory(CategoryVideoModel.Video_List category) {
            this.category = category;
        }

        public MyViewHolder(final Activity activity, @NonNull View itemView) {
            super(itemView);
            iv_product_thumb = (RoundRectCornerImageView) itemView.findViewById(R.id.iv_product_thumb);
            txt_video_title = (TextView) itemView.findViewById(R.id.txt_video_title);
            fab = (FloatingActionButton) itemView.findViewById(R.id.fab);
            fab_history = (FloatingActionButton) itemView.findViewById(R.id.fab_history);
            fab.setOnClickListener(CategoryVIdeoAdapter.this);
            fab_history.setOnClickListener(CategoryVIdeoAdapter.this);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fab) {
            CategoryVideoModel.Video_List _object = (CategoryVideoModel.Video_List) view.getTag();
            onItemClick.onSelectCategoryItem(_object);
        }
        if (view.getId() == R.id.fab_history) {
            Intent intent = new Intent(context, SurvayActivity.class);
            Gson _gson = new Gson();

            Type listOfdoctorType = new TypeToken<List<CategoryVideoModel.Video_List>>() {
            }.getType();
            String _videoList = _gson.toJson(productList, listOfdoctorType);
            intent.putExtra("videolist", _videoList);
            intent.putExtra("Categoryid", Categoryid);
            onItemClick.PauseVideoHere();

            context.startActivity(intent);
        }
    }


}
