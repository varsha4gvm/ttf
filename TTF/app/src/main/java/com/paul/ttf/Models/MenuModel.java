package com.paul.ttf.Models;

import android.graphics.drawable.Drawable;

public class MenuModel {

    String Menu_Item;
    Drawable icon;

    public String getMenu_Item() {
        return Menu_Item;
    }

    public void setMenu_Item(String menu_Item) {
        Menu_Item = menu_Item;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }
}
