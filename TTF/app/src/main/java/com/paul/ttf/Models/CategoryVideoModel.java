package com.paul.ttf.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CategoryVideoModel {


    @SerializedName("code")
    public Integer code;

    @SerializedName("message")
    public String message;

    @SerializedName("Video_List")
    public ArrayList<Video_List> Video_List;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CategoryVideoModel.Video_List> getVideo_List() {
        return Video_List;
    }

    public void setVideo_List(ArrayList<CategoryVideoModel.Video_List> video_List) {
        Video_List = video_List;
    }

    public static class Video_List implements Parcelable {

        @SerializedName("CategoryID")
        public Integer CategoryID;

        @SerializedName("CategoryName")
        public String CategoryName;

        @SerializedName("YoutubeVideoID")
        public String YoutubeVideoID;

        @SerializedName("VideoID")
        public int VideoID;

        @SerializedName("VideoName")
        public String VideoName;

        @SerializedName("VideoPath")
        public String VideoPath;

        @SerializedName("Title")
        public String Title;

        @SerializedName("ThumbnailPath")
        public String ThumbnailPath;


        public boolean isPause = false;

        public boolean isPause() {
            return isPause;
        }

        public void setPause(boolean pause) {
            isPause = pause;
        }

        @SerializedName("settimepopup")
        public ArrayList<settimepopup> settimepopup;


        public Integer getCategoryID() {
            return CategoryID;
        }

        public void setCategoryID(Integer categoryID) {
            CategoryID = categoryID;
        }

        public String getCategoryName() {
            return CategoryName;
        }

        public void setCategoryName(String categoryName) {
            CategoryName = categoryName;
        }

        public String getVideoPath() {
            return VideoPath;
        }

        public void setVideoPath(String videoPath) {
            VideoPath = videoPath;
        }

        public String getYoutubeVideoID() {
            return YoutubeVideoID;
        }

        public void setYoutubeVideoID(String youtubeVideoID) {
            YoutubeVideoID = youtubeVideoID;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public int getVideoID() {
            return VideoID;
        }

        public void setVideoID(int videoID) {
            VideoID = videoID;
        }

        public String getVideoName() {
            return VideoName;
        }

        public void setVideoName(String videoName) {
            VideoName = videoName;
        }

        public String getThumbnailPath() {
            return ThumbnailPath;
        }

        public void setThumbnailPath(String thumbnailPath) {
            ThumbnailPath = thumbnailPath;
        }

        public ArrayList<CategoryVideoModel.Video_List.settimepopup> getSettimepopup() {
            return settimepopup;
        }

        public void setSettimepopup(ArrayList<CategoryVideoModel.Video_List.settimepopup> settimepopup) {
            this.settimepopup = settimepopup;
        }

        protected Video_List(Parcel in) {
            VideoPath = in.readString();
            String[] c = new String[2];
            in.readStringArray(c);
            CategoryName = c[0];
            ThumbnailPath = c[1];
        }

        public static final Creator<CategoryVideoModel.Video_List> CREATOR = new Creator<CategoryVideoModel.Video_List>() {
            @Override
            public CategoryVideoModel.Video_List createFromParcel(Parcel in) {
                return new CategoryVideoModel.Video_List(in);
            }

            @Override
            public CategoryVideoModel.Video_List[] newArray(int size) {
                return new CategoryVideoModel.Video_List[size];
            }
        };


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(VideoPath);
            String[] c = new String[]{CategoryName, ThumbnailPath};
            parcel.writeStringArray(c);
        }

        public class settimepopup {

            @SerializedName("TimelineID")
            public int TimelineID;

            @SerializedName("Time")
            public String Time;

            @SerializedName("TestQueryID")
            public Integer TestQueryID;


            @SerializedName("PopUp_Text")
            public String PopUp_Text;

            @SerializedName("isInput")
            public boolean isInput;

            @SerializedName("isNewData")
            public boolean isNewData;


            @SerializedName("PopUp_Opacity")
            public float PopUp_Opacity;

            @SerializedName("PopUp_Duration")
            public int PopUp_Duration;


            long AdsSeconds = 0;

            public long getAdsSeconds() {
                return AdsSeconds;
            }

            public void setAdsSeconds(long adsSeconds) {
                AdsSeconds = adsSeconds;
            }

            @SerializedName("ButtonList")
            public ArrayList<ButtonList> ButtonList;

            public ArrayList<CategoryVideoModel.Video_List.settimepopup.ButtonList> getButtonList() {
                return ButtonList;
            }

            public void setButtonList(ArrayList<CategoryVideoModel.Video_List.settimepopup.ButtonList> buttonList) {
                ButtonList = buttonList;
            }

            public boolean isInput() {
                return isInput;
            }

            public void setInput(boolean input) {
                isInput = input;
            }

            public int getTimelineID() {
                return TimelineID;
            }

            public void setTimelineID(int timelineID) {
                TimelineID = timelineID;
            }

            public float getPopUp_Opacity() {
                return PopUp_Opacity;
            }

            public void setPopUp_Opacity(float popUp_Opacity) {
                PopUp_Opacity = popUp_Opacity;
            }

            public int getPopUp_Duration() {
                return PopUp_Duration;
            }

            public void setPopUp_Duration(int popUp_Duration) {
                PopUp_Duration = popUp_Duration;
            }

            public String getTime() {
                return Time;
            }

            public void setTime(String time) {
                Time = time;
            }

            public String getPopUp_Text() {
                return PopUp_Text;
            }

            public void setPopUp_Text(String popUp_Text) {
                PopUp_Text = popUp_Text;
            }

            public Integer getTestQueryID() {
                return TestQueryID;
            }

            public void setTestQueryID(Integer testQueryID) {
                TestQueryID = testQueryID;
            }

            public boolean isNewData() {
                return isNewData;
            }

            public void setNewData(boolean newData) {
                isNewData = newData;
            }

            public class ButtonList {

                @SerializedName("ButtonID")
                public int ButtonID;


                @SerializedName("ButtonName")
                public String ButtonName;

                @SerializedName("ButtonColor")
                public String ButtonColor;

                public int getButtonID() {
                    return ButtonID;
                }

                public void setButtonID(int buttonID) {
                    ButtonID = buttonID;
                }

                public String getButtonName() {
                    return ButtonName;
                }

                public void setButtonName(String buttonName) {
                    ButtonName = buttonName;
                }

                public String getButtonColor() {
                    return ButtonColor;
                }

                public void setButtonColor(String buttonColor) {
                    ButtonColor = buttonColor;
                }
            }
        }

    }
}
