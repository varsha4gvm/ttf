package com.paul.ttf.CustomControl.YoutubeExtractor;

public class ExtractorException extends Exception
{
	public ExtractorException(String msg){
		super(msg);
	}
}
