package com.paul.ttf.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.paul.ttf.Models.CategoryVideoModel;
import com.paul.ttf.R;

import java.util.ArrayList;


public class CustomVideoAdapter extends ArrayAdapter<CategoryVideoModel.Video_List> {

    LayoutInflater flater;

    public CustomVideoAdapter(Activity context, int resouceId, int textviewId, ArrayList<CategoryVideoModel.Video_List> list) {

        super(context, resouceId, textviewId, list);
        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        return rowview(convertView, position);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        return rowview(convertView, position);

    }


    private View rowview(View convertView, int position) {

        if (convertView == null) {
            convertView = flater.inflate(R.layout.rowitem_layout, null, false);
        }

        CategoryVideoModel.Video_List rowItem = getItem(position);

        View rowview = flater.inflate(R.layout.rowitem_layout, null, true);

        TextView txt_label = (TextView) rowview.findViewById(R.id.txt_label);
        txt_label.setText(rowItem.getVideoName());
        txt_label.setTag(rowItem);
        return rowview;
    }
}
