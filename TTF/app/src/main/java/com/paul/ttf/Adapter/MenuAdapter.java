package com.paul.ttf.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.paul.ttf.InterfaceModule.MenuRedirect;
import com.paul.ttf.Models.MenuModel;
import com.paul.ttf.R;
import com.paul.ttf.databinding.LeftItemLayoutBinding;

import java.util.ArrayList;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MyViewHolder> implements View.OnClickListener {

    ArrayList<MenuModel> productList;
    Activity context;
    MenuRedirect onItemClick;

    public MenuAdapter(ArrayList<MenuModel> productList, Activity context, MenuRedirect onItemClick) {
        this.productList = productList;
        this.context = context;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        //View view;

        View v = LayoutInflater.from(context).inflate(R.layout.menu_item_layout, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(v);
        return viewHolder;


    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int position) {
        final MenuModel _object = productList.get(position);

        myViewHolder.linear_parent.setTag(_object);
        myViewHolder.img_icon.setImageDrawable(_object.getIcon());
        myViewHolder.txt_title.setText(_object.getMenu_Item());

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.linear_parent) {
            MenuModel _object = (MenuModel) view.getTag();
            onItemClick.MenuRedirectEvent(_object);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private static final String LOG_TAG = "Holder";

        ImageView img_icon;
        TextView txt_title;
        LinearLayout linear_parent;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            img_icon = (ImageView) itemView.findViewById(R.id.img_icon);
            txt_title = (TextView) itemView.findViewById(R.id.txt_title);
            linear_parent = (LinearLayout) itemView.findViewById(R.id.linear_parent);

            linear_parent.setOnClickListener(MenuAdapter.this);

        }


    }

}
