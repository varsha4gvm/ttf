package com.paul.ttf.APIThread;

import com.paul.ttf.Models.AllCategoryVideoModel;
import com.paul.ttf.Models.CategoryListModel;
import com.paul.ttf.Models.CategoryVideoModel;
import com.paul.ttf.Models.LoginRequestModel;
import com.paul.ttf.Models.LoginResponseModel;
import com.paul.ttf.Models.SurvayRequestModel;
import com.paul.ttf.Models.SurvayResponseModel;
import com.paul.ttf.Models.UsereviewRequestModel;
import com.paul.ttf.Models.UsereviewResponseModel;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;


public interface APIInterface {

    /*App API*/
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @GET
    public Observable<CategoryListModel> getCategoryList(@Url String url);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @GET
    public Observable<CategoryVideoModel> getCategoryVideoList(@Url String url);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @GET
    public Observable<AllCategoryVideoModel> getAllCategoryVideoList(@Url String url);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("/api/review/UserReview")
    Observable<SurvayResponseModel> TakeSurvay(@Body SurvayRequestModel productrequest);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("/api/Login/LoginUser")
    Observable<LoginResponseModel> UserLogin(@Body LoginRequestModel productrequest);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("/api/getreview/GetUserreviews")
    Observable<UsereviewResponseModel> GetUserSurvay(@Body UsereviewRequestModel productrequest);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @GET
    public Observable<AllCategoryVideoModel> CheckSubscription(@Url String url);


}
