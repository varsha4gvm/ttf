package com.paul.ttf.InterfaceModule;

import com.paul.ttf.Models.CategoryVideoModel;

public interface DialogPreviewCallBack {

    void callbackObserver(CategoryVideoModel.Video_List.settimepopup callback, Integer VideoID, Integer CategoryID, Integer TestQueryID, boolean IsEntryData, String VideoName);

    void NONcallbackObserver();

    void ForceHide(boolean isMemeber);

}
