package com.paul.ttf.UI;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.paul.ttf.R;
import com.paul.ttf.Utility.SharedPreferencesUtility;

public class ProfileActivity extends AppCompatActivity {


    SharedPreferencesUtility _shared_prefrance;
    EditText edit_firstname, edit_lastname, edit_emailid, edit_doj;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_profile_screen);

        setUpNavigationView();
        _shared_prefrance = new SharedPreferencesUtility(ProfileActivity.this);
        init();
    }

    private void setUpNavigationView() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // perform whatever you want on back arrow click
                onBackPressed();
            }
        });
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu


        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
    }

    private void init() {

        edit_firstname = findViewById(R.id.edit_firstname);
        edit_lastname = findViewById(R.id.edit_lastname);
        edit_emailid = findViewById(R.id.edit_emailid);
        edit_doj = findViewById(R.id.edit_doj);

        edit_firstname.setText(_shared_prefrance.getFirstname());
        edit_lastname.setText(_shared_prefrance.getSurname());
        edit_emailid.setText(_shared_prefrance.getEmailid());
        edit_doj.setText(_shared_prefrance.getJoindate());

    }


}
