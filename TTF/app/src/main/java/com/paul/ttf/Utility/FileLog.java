package com.paul.ttf.Utility;

import android.util.Log;

public class FileLog {

    static boolean isdisplayLogs = true;

    public static void e(String tag, String msg) {
        if (isdisplayLogs) {
            Log.e(tag, msg);
        }
    }

    public static void e(String tag, String msg, Throwable tr) {
        if (isdisplayLogs) {
            Log.e(tag, msg, tr);
        }
    }
}
