package com.paul.ttf;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.paul.ttf.APIThread.APIClient;
import com.paul.ttf.APIThread.APIInterface;
import com.paul.ttf.Adapter.CategoryListAdapter;
import com.paul.ttf.Adapter.ItemOffsetDecoration;
import com.paul.ttf.Adapter.MenuAdapter;
import com.paul.ttf.InterfaceModule.CategoryInterface;
import com.paul.ttf.InterfaceModule.MenuRedirect;
import com.paul.ttf.Models.AllCategoryVideoModel;
import com.paul.ttf.Models.CategoryListModel;
import com.paul.ttf.Models.LoginRequestModel;
import com.paul.ttf.Models.LoginResponseModel;
import com.paul.ttf.Models.MenuModel;
import com.paul.ttf.UI.ProfileActivity;
import com.paul.ttf.UI.SplashActivity;
import com.paul.ttf.UI.SurvayActivity;
import com.paul.ttf.Utility.CommonFunction;
import com.paul.ttf.Utility.Constants;
import com.paul.ttf.Utility.FileLog;
import com.paul.ttf.Utility.SharedPreferencesUtility;
import com.paul.ttf.databinding.ActivityHomeScreenBinding;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class HomeActivity extends AppCompatActivity implements CategoryInterface, View.OnClickListener, MenuRedirect {


    public static final String CATEGORY = "category";


    CategoryListAdapter adapter;
    ArrayList<CategoryListModel.CategoryList> _category_list;
    private ActivityHomeScreenBinding binding;
    SharedPreferencesUtility _shared_prefrance;

    MenuItem item;

    private CompositeDisposable disposables = new CompositeDisposable();

    //private LoadingView mLoadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setContentView(R.layout.activity_home_screen);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home_screen);
        _shared_prefrance = new SharedPreferencesUtility(HomeActivity.this);
        _category_list = new ArrayList<>();


        init();

    }

    private void init() {


        //binding.homemain.toolbar.inflateMenu(R.menu.main);
        setSupportActionBar(binding.homemain.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        binding.homemain.toolbar.setTitle("");
        binding.homemain.toolbar.setNavigationIcon(R.drawable.ic_menu_white);

        binding.homemain.linearSurvay.setOnClickListener(this);

        binding.homemain.recyclerviewCategoryList.setNestedScrollingEnabled(false);
        //fillList();
        binding.homemain.recyclerviewCategoryList.setLayoutManager(new LinearLayoutManager(HomeActivity.this));
        /*Recycleview Animation*/
      /*  int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(HomeActivity.this, resId);
        binding.recyclerviewCategoryList.setLayoutAnimation(animation);*/

        if (CommonFunction.isInternetAvailable(HomeActivity.this)) {

            Log.e("TAGGG", "'Date match condy > " + _shared_prefrance.getCurrentDate() + " current " + CommonFunction.getCurrentDate() + " Test " + (!_shared_prefrance.getCurrentDate().equals(CommonFunction.getCurrentDate())));

            if ((!_shared_prefrance.getToken().equals("")) && !_shared_prefrance.getCurrentDate().equals(CommonFunction.getCurrentDate())) {
                GetLoginForSubscriptionData();
            } else {
                GetCategorydata();
            }


        } else {
            Snackbar snackbar = Snackbar
                    .make(binding.coordinateParent, getString(R.string.error_internet), Snackbar.LENGTH_LONG);

            snackbar.setActionTextColor(Color.RED);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        }
        ConfigureProgress();
        binding.homemain.loadingViewRepeat.setOnClickListener(this);
        //recyclerview_categoryList.smoot

        setUpNavigationView();
        ConfigureMenu();

    }


    private void ConfigureMenu() {


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(HomeActivity.this, RecyclerView.VERTICAL, false);
        binding.recycleList.setLayoutManager(mLayoutManager);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(HomeActivity.this, R.dimen.item_offset);
        binding.recycleList.addItemDecoration(itemDecoration);

        MenuAdapter _menuadapter;
        if (_shared_prefrance.getMemberid() != 0) {
            if (_shared_prefrance.getGender().toLowerCase().equals("m")) {
                binding.linearHeader.ivPersonIconGc.setImageResource(R.drawable.male);
            } else {
                binding.linearHeader.ivPersonIconGc.setImageResource(R.drawable.female);
            }
            binding.linearHeader.textName.setText(_shared_prefrance.getFirstname() + " " + _shared_prefrance.getSurname());

            _menuadapter = new MenuAdapter(GetMenuList(), HomeActivity.this, this);
        } else {
            _menuadapter = new MenuAdapter(GetSampleMenuList(), HomeActivity.this, this);
        }

        binding.recycleList.setAdapter(_menuadapter);

    }

    private ArrayList<MenuModel> GetMenuList() {
        ArrayList<MenuModel> _list = new ArrayList<>();
        MenuModel _object_profile = new MenuModel();
        _object_profile.setIcon(getResources().getDrawable(R.drawable.man));
        _object_profile.setMenu_Item(getResources().getString(R.string.profile));
        _list.add(_object_profile);
        MenuModel _object_logout = new MenuModel();
        _object_logout.setIcon(getResources().getDrawable(R.drawable.logout));
        _object_logout.setMenu_Item(getResources().getString(R.string.signout));
        _list.add(_object_logout);
        return _list;
    }

    private ArrayList<MenuModel> GetSampleMenuList() {
        ArrayList<MenuModel> _list = new ArrayList<>();

        MenuModel _object_logout = new MenuModel();
        _object_logout.setIcon(getResources().getDrawable(R.drawable.signin));
        _object_logout.setMenu_Item(getResources().getString(R.string.signin));
        _list.add(_object_logout);
        return _list;
    }


    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        binding.navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.home:

                        break;


                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);


                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, binding.coordinateParent, binding.homemain.toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };


        //Setting the actionbarToggle to drawer layout
        binding.coordinateParent.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
        binding.homemain.toolbar.setNavigationIcon(R.drawable.ic_menu_white);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onSelectCategoryItem(int position) {
        Intent intent = new Intent();
        intent.setClass(this, CategoryDetailActivity.class);

    }

    private void GetCategorydata() {


        APIInterface gerritAPI;
        int IsGuest = 1;
        if (!_shared_prefrance.getToken().equals("")) {
            gerritAPI = APIClient.getClientHeader(_shared_prefrance.getToken()).create(APIInterface.class);
            IsGuest = 0;
        } else {
            gerritAPI = APIClient.getClientHeader(Constants.Guest_Token).create(APIInterface.class);
        }


        String Category_uri = Constants.BASE_URL + Constants.GET_CATEGORY_URL + "?CategoryID=0&IsGuest=" + IsGuest;
        Observable<CategoryListModel> profileResponse = gerritAPI.getCategoryList(Category_uri);
        doProgressStart();
        profileResponse.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<CategoryListModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(CategoryListModel categoryListModel) {


                if (categoryListModel.getCode() == 200) {
                    FileLog.e("TAGGG", "Number converter String  here onNext>> " + categoryListModel.getCategoryList().size());
                    _category_list = categoryListModel.getCategoryList();
                    fillList();
                } else if (categoryListModel.getCode() == 401) {

                    SharedPreferencesUtility _sharePreference = new SharedPreferencesUtility(HomeActivity.this);
                    _sharePreference.ClearPrefrance();


                    Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("islogout", "yes");
                    startActivity(intent);
                    finish();

                    //ShowAuthError(categoryListModel.getMessage(), getString(R.string.error_auth));

                } else {
                    Snackbar snackbar = Snackbar
                            .make(binding.coordinateParent, categoryListModel.getMessage(), Snackbar.LENGTH_LONG);

                    snackbar.setActionTextColor(Color.RED);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                }

            }

            @Override
            public void onError(Throwable e) {
                DismissProgress();
            }

            @Override
            public void onComplete() {
                DismissProgress();
                FileLog.e("TAGGG", "Number converter String  here onComplete>> ");
            }
        });

    }

    private void fillList() {

        adapter = new CategoryListAdapter(_category_list, HomeActivity.this, this);

        binding.homemain.recyclerviewCategoryList.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            FileLog.e("TAGGG", "Number converter String Call 1");
            disposables.clear(); // clearing it : do not emit after destroy
            binding.homemain.loadingViewRepeat.setVisibility(View.GONE);

        } catch (Exception e) {

        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.linear_survay) {
            Intent intent = new Intent(HomeActivity.this, SurvayActivity.class);
            startActivity(intent);
        }
    }

    private void doProgressStart() {

        binding.homemain.loadingViewRepeat.setVisibility(View.VISIBLE);
        disposables.add(getObservable()
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getObserver()));
    }

    private DisposableObserver<Long> getObserver() {
        return new DisposableObserver<Long>() {

            @Override
            public void onNext(Long value) {
                binding.homemain.loadingViewRepeat.startAnimation();
            }

            @Override
            public void onError(Throwable e) {
                DismissProgress();
            }

            @Override
            public void onComplete() {
                DismissProgress();
            }
        };
    }

    private void ConfigureProgress() {

        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        int marvel_1 = R.drawable.marvel_1_lollipop;
        int marvel_2 = R.drawable.marvel_2_lollipop;
        int marvel_3 = R.drawable.marvel_3_lollipop;
        int marvel_4 = R.drawable.marvel_4_lollipop;
        binding.homemain.loadingViewRepeat.addAnimation(Color.parseColor("#FFD200"), marvel_1,
                LoadingView.FROM_LEFT);
        binding.homemain.loadingViewRepeat.addAnimation(Color.parseColor("#2F5DA9"), marvel_2,
                LoadingView.FROM_TOP);
        binding.homemain.loadingViewRepeat.addAnimation(Color.parseColor("#FF4218"), marvel_3,
                LoadingView.FROM_RIGHT);
        binding.homemain.loadingViewRepeat.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);

        binding.homemain.loadingViewRepeat.addListener(new LoadingView.LoadingListener() {
            @Override
            public void onAnimationStart(int currentItemPosition) {
                FileLog.e("TAGGG", "Number converter String Call > onAnimationStart");
            }

            @Override
            public void onAnimationRepeat(int nextItemPosition) {
                FileLog.e("TAGGG", "Number converter String Call > onAnimationRepeat");
            }

            @Override
            public void onAnimationEnd(int nextItemPosition) {
                FileLog.e("TAGGG", "Number converter String Call > onAnimationEnd");
            }
        });
    }

    private void DismissProgress() {
        FileLog.e("TAGGG", "Number converter String Call 2");
        binding.homemain.loadingViewRepeat.setVisibility(View.GONE);
        disposables.clear(); // clearing it : do not emit after destroy
    }

    private Observable<? extends Long> getObservable() {
        return Observable.interval(0, 500, TimeUnit.MILLISECONDS);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.main, menu);

        //item = menu.findItem(R.id.action_logout);
        //item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            //ShowAlertOption();
        }


        return super.onOptionsItemSelected(item);
    }

    public boolean ShowAlertOption() {


        TextView title = new TextView(this);
        title.setText("Logout?");
        title.setPadding(15, 15, 15, 10);
        title.setGravity(Gravity.LEFT);
// title.setTextColor(getResources().getColor(R.color.greenBG));
        title.setTextSize(18);

        TextView msg = new TextView(this);
        msg.setText(getString(R.string.txt_delete_confirm));
        msg.setPadding(15, 10, 15, 10);
        msg.setGravity(Gravity.LEFT);
        msg.setTextSize(16);

        DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) { //Delete for me

                    SharedPreferencesUtility _sharePreference = new SharedPreferencesUtility(HomeActivity.this);
                    _sharePreference.ClearPrefrance();


                    Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                    dialog.dismiss();
                }
                if (which == DialogInterface.BUTTON_NEGATIVE) {//Delete for everyone
                    FileLog.e("TAGGGG", "DialogInterface.OnClickListener BUTTON_NEGATIVE");
                    dialog.dismiss();
                }
                if (which == DialogInterface.BUTTON_NEUTRAL) {//Cancel
                    FileLog.e("TAGGGG", "DialogInterface.OnClickListener BUTTON_NEUTRAL");
                    dialog.dismiss();
                }
            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCustomTitle(title);
        builder.setView(msg);
        builder.setCancelable(true);
        builder.setPositiveButton(getString(R.string.txt_logout), onClick);
        builder.setNegativeButton(getString(R.string.txt_delete_cancel), onClick);


        AlertDialog dialog = builder.create();
        dialog.show();
        return true;
    }


    public boolean ShowAuthError(String msg_title, String msg_detail) {


        TextView title = new TextView(this);
        title.setText(msg_title);
        title.setPadding(15, 15, 15, 10);
        title.setGravity(Gravity.LEFT);
// title.setTextColor(getResources().getColor(R.color.greenBG));
        title.setTextSize(18);

        TextView msg = new TextView(this);
        msg.setText(msg_detail);
        msg.setPadding(15, 10, 15, 10);
        msg.setGravity(Gravity.LEFT);
        msg.setTextSize(16);

        DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) { //Delete for me


                }

            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCustomTitle(title);
        builder.setView(msg);
        //builder.setCancelable(true);
        builder.setPositiveButton(getString(R.string.txt_logout), onClick);
        //builder.setNegativeButton(getString(R.string.txt_delete_cancel), onClick);


        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        return true;
    }

    @Override
    public void MenuRedirectEvent(MenuModel object) {
        if (object.getMenu_Item().equals(getResources().getString(R.string.profile))) {
            binding.coordinateParent.closeDrawer(GravityCompat.START);
            Intent _Intent = new Intent(HomeActivity.this, ProfileActivity.class);
            startActivity(_Intent);
        } else if (object.getMenu_Item().equals(getResources().getString(R.string.signout))) {
            binding.coordinateParent.closeDrawer(GravityCompat.START);
            ShowAlertOption();
        } else if (object.getMenu_Item().equals(getResources().getString(R.string.signin))) {
            Intent _Intent = new Intent(HomeActivity.this, SplashActivity.class);
            startActivity(_Intent);
            finish();
        }
    }


    private void GetLoginForSubscriptionData() {

        if (!_shared_prefrance.getToken().equals("")) {
            APIInterface gerritAPI = APIClient.getClient().create(APIInterface.class);

            LoginRequestModel _model = new LoginRequestModel();
            _model.setPassword(_shared_prefrance.getPassword());
            _model.setUserName(_shared_prefrance.getEmailid());
            _model.setDeviceId(CommonFunction.getDeviceUUID());
            _model.setDeviceName(CommonFunction.getDeviceName());
            _model.setVersionNo(CommonFunction.getAppVersion());

            Observable<LoginResponseModel> profileResponse = gerritAPI.UserLogin(_model);
            //doProgressStart();
            profileResponse.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<LoginResponseModel>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(LoginResponseModel categoryListModel) {


                    if (categoryListModel.getCode() == 200) {
                        FileLog.e("TAGGG", "Number converter String  here onNext>> " + categoryListModel.getLoginInfo());

                        _shared_prefrance.setToken(categoryListModel.getLoginInfo().getToken());
                        _shared_prefrance.setCurrentDate(CommonFunction.getCurrentDate());

                        GetCategorydata();
                    } else if (categoryListModel.getCode() == 202) {

                        SharedPreferencesUtility _sharePreference = new SharedPreferencesUtility(HomeActivity.this);
                        _sharePreference.ClearPrefrance();


                        Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("islogout", "yes");
                        startActivity(intent);
                        finish();

                    }

                }

                @Override
                public void onError(Throwable e) {
                }

                @Override
                public void onComplete() {

                    FileLog.e("TAGGG", "Number converter String  here onComplete>> ");
                }
            });
        }

    }


}
