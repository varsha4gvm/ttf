package com.paul.ttf.Models;

import com.google.gson.annotations.SerializedName;

public class SurvayResponseModel {


    @SerializedName("code")
    public Integer code;

    @SerializedName("message")
    public String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
