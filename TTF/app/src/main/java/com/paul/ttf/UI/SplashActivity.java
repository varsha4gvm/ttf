package com.paul.ttf.UI;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.paul.ttf.APIThread.APIClient;
import com.paul.ttf.APIThread.APIInterface;
import com.paul.ttf.HomeActivity;
import com.paul.ttf.LoadingView;
import com.paul.ttf.Models.LoginRequestModel;
import com.paul.ttf.Models.LoginResponseModel;
import com.paul.ttf.R;
import com.paul.ttf.Utility.CommonFunction;
import com.paul.ttf.Utility.FileLog;
import com.paul.ttf.Utility.SharedPreferencesUtility;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    private static int SPLASH_TIME_OUT = 2000;

    Handler handler = new Handler();
    Runnable runnable;

    ImageView img_spalsh, img_logo;
    LinearLayout linear_login;
    boolean ishide = false;
    private LoadingView mLoadingView;
    Button btn_login, btn_signup;

    private final CompositeDisposable disposables = new CompositeDisposable();
    LinearLayout linear_loading;
    EditText edit_emailid, edit_password;
    RelativeLayout relative_parent;
    SharedPreferencesUtility _shared_prefrance;
    TextView txt_skip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        _shared_prefrance = new SharedPreferencesUtility(SplashActivity.this);
        if (getIntent().hasExtra("subscribe")) {
            if (getIntent().getStringExtra("subscribe").equals("yes")) {
                Intent i = new Intent(SplashActivity.this, WebviewActivity.class);
                startActivity(i);
            }
        }
        init();
    }


    private void init() {

        img_logo = findViewById(R.id.img_logo);
        img_spalsh = findViewById(R.id.img_spalsh);
        linear_login = findViewById(R.id.linear_login);
        btn_login = findViewById(R.id.btn_login);
        btn_signup = findViewById(R.id.btn_signup);
        txt_skip = findViewById(R.id.txt_skip);
        relative_parent = findViewById(R.id.relative_parent);
        linear_loading = findViewById(R.id.linear_loading);
        mLoadingView = (LoadingView) findViewById(R.id.loading_view_repeat);
        edit_emailid = findViewById(R.id.edit_emailid);
        edit_password = findViewById(R.id.edit_password);

        //txt_skip.setPaintFlags(txt_skip.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        btn_login.setOnClickListener(this);
        btn_signup.setOnClickListener(this);
        txt_skip.setOnClickListener(this);
        linear_loading.setOnClickListener(this);

        /*Glide.with(SplashActivity.this)
                .load(R.drawable.bg_screen)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.bg_screen)
                        .fitCenter())
                .into(img_spalsh);*/

        linear_login.setVisibility(View.GONE);
        txt_skip.setVisibility(View.GONE);
        redirectionAfterTimeOut();

        ConfigureProgress();
        if (getIntent().hasExtra("islogout")) {
            if (getIntent().getStringExtra("islogout").equals("yes")) {
                ShowAlertOption(getString(R.string.txt_logout), getString(R.string.error_auth));
            }
        }
    }

    void redirectionAfterTimeOut() {
        runnable = new Runnable() {

            @Override
            public void run() {

                if (_shared_prefrance.getMemberid() != 0) {
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                } else {

                    FadeOutAnim(img_logo);
                    handler.removeCallbacks(this);
                }
                /**/
                // doProgressStart();


            }
        };
        handler.postDelayed(runnable, SPLASH_TIME_OUT);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (handler != null) {
                disposables.clear(); // clearing it : do not emit after destroy
                linear_loading.setVisibility(View.GONE);
                handler.removeCallbacks(runnable);
            }
        } catch (Exception e) {

        }
    }

    private void FadeOutAnim(final ImageView image_view) {

        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(image_view, "alpha", 1f, 0f);
        fadeIn.setDuration(800);
        final AnimatorSet mAnimationSet = new AnimatorSet();
        mAnimationSet.play(fadeIn);
        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationEnd(animation);


            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                image_view.setVisibility(View.GONE);
                FadeInAnim(linear_login);
            }
        });
        mAnimationSet.start();

        ishide = false;
    }

    private void FadeInAnim(final LinearLayout Linear_view) {
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(Linear_view, "alpha", .0f, 1f);
        fadeOut.setDuration(800);


        final AnimatorSet mAnimationSet = new AnimatorSet();
        mAnimationSet.play(fadeOut);
        mAnimationSet.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                Linear_view.setVisibility(View.VISIBLE);
                txt_skip.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

            }


        });


        mAnimationSet.start();

        //image_message_share.setVisibility(View.VISIBLE);
        //image_message_reply.setVisibility(View.VISIBLE);

        ishide = true;
    }

    private void ConfigureProgress() {


        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        int marvel_1 = R.drawable.marvel_1_lollipop;
        int marvel_2 = R.drawable.marvel_2_lollipop;
        int marvel_3 = R.drawable.marvel_3_lollipop;
        int marvel_4 = R.drawable.marvel_4_lollipop;
        mLoadingView.addAnimation(Color.parseColor("#FFD200"), marvel_1,
                LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#2F5DA9"), marvel_2,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#FF4218"), marvel_3,
                LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);

        mLoadingView.addListener(new LoadingView.LoadingListener() {
            @Override
            public void onAnimationStart(int currentItemPosition) {

            }

            @Override
            public void onAnimationRepeat(int nextItemPosition) {

            }

            @Override
            public void onAnimationEnd(int nextItemPosition) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_login) {
            if (IsValid()) {
                GetUserLogindata();
            }
        }
        if (view.getId() == R.id.btn_signup) {
            Intent i = new Intent(SplashActivity.this, WebviewActivity.class);
            startActivity(i);

        }
        if (view.getId() == R.id.linear_loading) {
            linear_loading.setVisibility(View.GONE);
            disposables.clear(); // clearing it : do not emit after destroy
        }
        if (view.getId() == R.id.txt_skip) {
            Intent i = new Intent(SplashActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
        }
    }


    private Observable<? extends Long> getObservable() {
        return Observable.interval(0, 500, TimeUnit.MILLISECONDS);
    }

    private void doProgressStart() {
        linear_loading.setVisibility(View.VISIBLE);
        disposables.add(getObservable()
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getObserver()));
    }

    private DisposableObserver<Long> getObserver() {
        return new DisposableObserver<Long>() {

            @Override
            public void onNext(Long value) {
                mLoadingView.startAnimation();
                Log.d("TAGGG", " onNext : value : " + value);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
               /* textView.append(" onComplete");
                textView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, " onComplete");*/
            }
        };
    }

    private boolean IsValid() {
        boolean isvalid = true;
        if (edit_emailid.getText().toString().trim().equals("")) {
            edit_emailid.setError(getString(R.string.required_email));
            edit_emailid.setFocusable(true);
            isvalid = false;
        } else if (edit_password.getText().toString().trim().equals("")) {
            edit_password.setError(getString(R.string.required_password));
            edit_password.setFocusable(true);
            isvalid = false;
        }

        return isvalid;
    }

    private void GetUserLogindata() {

        APIInterface gerritAPI = APIClient.getClient().create(APIInterface.class);

        LoginRequestModel _model = new LoginRequestModel();
        _model.setPassword(edit_password.getText().toString());
        _model.setUserName(edit_emailid.getText().toString());
        _model.setDeviceId(CommonFunction.getDeviceUUID());
        _model.setDeviceName(CommonFunction.getDeviceName());
        _model.setVersionNo(CommonFunction.getAppVersion());

        Observable<LoginResponseModel> profileResponse = gerritAPI.UserLogin(_model);
        doProgressStart();
        profileResponse.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<LoginResponseModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(LoginResponseModel categoryListModel) {


                if (categoryListModel.getCode() == 200) {
                    FileLog.e("TAGGG", "Number converter String  here onNext>> " + categoryListModel.getLoginInfo());

                    _shared_prefrance.setToken(categoryListModel.getLoginInfo().getToken());
                    _shared_prefrance.setPassword(edit_password.getText().toString());
                    _shared_prefrance.setMemberid(categoryListModel.getLoginInfo().getMemberid());
                    _shared_prefrance.setEmailid(categoryListModel.getLoginInfo().getEmail());
                    _shared_prefrance.setFirstname(categoryListModel.getLoginInfo().getFirstname());
                    _shared_prefrance.setSurname(categoryListModel.getLoginInfo().getSurname());
                    _shared_prefrance.setGender(categoryListModel.getLoginInfo().getGender());
                    _shared_prefrance.setJoindate(categoryListModel.getLoginInfo().getJoindate());
                    _shared_prefrance.setMemberphoto(categoryListModel.getLoginInfo().getMemberphoto());
                    _shared_prefrance.setCurrentDate(CommonFunction.getCurrentDate());


                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                } else if (categoryListModel.getCode() == 202) {
                    ShowAlertOption("Subscription Issue", categoryListModel.getMessage());
                } else {
                    Snackbar snackbar = Snackbar
                            .make(relative_parent, categoryListModel.getMessage(), Snackbar.LENGTH_LONG);

                    snackbar.setActionTextColor(Color.RED);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();


                    /*_shared_prefrance.setMemberid(5195);
                    _shared_prefrance.setEmailid("anvesh@gvmtechnologies.com");
                    _shared_prefrance.setFirstname("Anvesh");
                    _shared_prefrance.setSurname("Prajapati");
                    _shared_prefrance.setGender("m");
                    _shared_prefrance.setJoindate("09-30-2019");
                    _shared_prefrance.setMemberphoto("");


                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();*/
                }

            }

            @Override
            public void onError(Throwable e) {
                DismissProgress();
            }

            @Override
            public void onComplete() {
                DismissProgress();
                FileLog.e("TAGGG", "Number converter String  here onComplete>> ");
            }
        });

    }

    private void DismissProgress() {
        FileLog.e("TAGGG", "Number converter String Call 2");
        linear_loading.setVisibility(View.GONE);
        disposables.clear(); // clearing it : do not emit after destroy
    }

    public boolean ShowAlertOption(String Title, String Message) {

        TextView title = new TextView(this);
        title.setText(Title);
        title.setPadding(15, 15, 15, 10);
        title.setGravity(Gravity.LEFT);
// title.setTextColor(getResources().getColor(R.color.greenBG));
        title.setTextSize(18);

        TextView msg = new TextView(this);
        msg.setText(Message);
        msg.setPadding(15, 10, 15, 10);
        msg.setGravity(Gravity.LEFT);
        msg.setTextSize(16);

        DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) { //Delete for me

                    dialog.dismiss();
                }

            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCustomTitle(title);
        builder.setView(msg);
        //builder.setCancelable(true);
        builder.setPositiveButton(getString(R.string.txt_ok), onClick);
        //builder.setNegativeButton(getString(R.string.txt_delete_cancel), onClick);


        AlertDialog dialog = builder.create();
        dialog.show();
        return true;
    }


}
