package com.paul.ttf.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.databinding.DataBindingUtil;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.RecyclerView;

import com.paul.ttf.CategoryDetailActivity;
import com.paul.ttf.InterfaceModule.MenuRedirect;
import com.paul.ttf.Models.CategoryListModel;
import com.paul.ttf.R;

import com.paul.ttf.databinding.LeftItemLayoutBinding;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.target.ImageViewTarget;

import java.util.ArrayList;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.MyViewHolder> {

    ArrayList<CategoryListModel.CategoryList> productList;
    Activity context;
    MenuRedirect onItemClick;

    public CategoryListAdapter(ArrayList<CategoryListModel.CategoryList> productList, Activity context, MenuRedirect onItemClick) {
        this.productList = productList;
        this.context = context;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        //View view;

        MyViewHolder viewHolder = null;
        Log.e("TAGGG", "onCreateViewHolder POSI > " + i);

        LeftItemLayoutBinding inflate = DataBindingUtil
                .inflate(LayoutInflater
                        .from(parent.getContext()), R.layout.left_item_layout, parent, false);
        viewHolder = new MyViewHolder(context, inflate.getRoot());


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int position) {
        final CategoryListModel.CategoryList _object = productList.get(position);
        myViewHolder.set_category(_object);
        /*myViewHolder.txt_category_name.setText(_object.getName());
        myViewHolder.setCategory(_object);
        if (_object.getImages() != null) {
            Glide.with(ApplicationClass.applicationContext)
                    .load(_object.getImages())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.no_image)
                            .fitCenter())
                    .into(myViewHolder.iv_product_thumb);


        }*/

        if (position % 2 == 0) {
            myViewHolder.l_Binding.txtCategoryNameLeft.setVisibility(View.VISIBLE);
            myViewHolder.l_Binding.txtCategoryNameRight.setVisibility(View.GONE);
            myViewHolder.l_Binding.txtCategoryNameLeft.setText(_object.getCategoryName());

        } else {
            myViewHolder.l_Binding.txtCategoryNameLeft.setVisibility(View.GONE);
            myViewHolder.l_Binding.txtCategoryNameRight.setVisibility(View.VISIBLE);
            myViewHolder.l_Binding.txtCategoryNameRight.setText(_object.getCategoryName());

        }

        /*String last_Data = "";
        try {
            Log.e("TAGGG", "Check deeplink onClick :- " + _object.getImagePath());
            String[] split = _object.getImagePath().split("/");
            last_Data = split[split.length - 1];
            Log.e("TAGGG", "Check deeplink Last :- " + last_Data);
        } catch (Exception ex) {

        }*/


        Glide.with(context)
                .load(_object.getImagePath())
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .fitCenter()
                .placeholder(R.drawable.ttf_black)
                .error(R.drawable.ttf_black)
                .into(new ImageViewTarget<GlideDrawable>(myViewHolder.l_Binding.ivProductThumb) {
                    @Override
                    protected void setResource(GlideDrawable resource) {
                        setImage(resource);

                       /* if (!hasExtractedColorAlready()) {
                            extractColors(resource);
                        }*/
                    }

                   /* private boolean hasExtractedColorAlready() {
                        return movie.getColor() != 0;
                    }*/

                    private void setImage(GlideDrawable resource) {
                        myViewHolder.l_Binding.ivProductThumb.setImageDrawable(resource.getCurrent());
                    }

                    private void extractColors(GlideDrawable resource) {
                        Bitmap b = ((GlideBitmapDrawable) resource.getCurrent()).getBitmap();
                        Palette p = Palette.from(b).generate();

                        extractBackgroundColor(p);

//                        extractTextColor(p);
                    }

                    private void extractBackgroundColor(Palette p) {
                        int defaultColor = context.getResources().getColor(R.color.colorPrimary);


                        //                        int color = p.getDominantColor(defaultColor);
                        //                        int color = p.getMutedColor(defaultColor);
                        //                        int color = p.getLightMutedColor(defaultColor);
                        //                        int color = p.getDarkMutedColor(defaultColor);
                        //                        int color = p.getVibrantColor(defaultColor);
                        //                        int color = p.getLightVibrantColor(defaultColor);
                        int color = p.getDarkVibrantColor(defaultColor);
                        //movie.setBackgroundColor(color);
                    }

                    private void extractTextColor(Palette p) {
                        Palette.Swatch swatch = p.getDarkVibrantSwatch();
                       /* int textColor = Color.parseColor("#ffffff");
                        if (swatch != null) {
                            textColor = swatch.getBodyTextColor();
                        }*/
                        // movie.setTextColor(textColor);
                    }
                });


    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private static final String LOG_TAG = "Holder";
        private CategoryListModel category;
        //ImageView iv_product_thumb;
        //TextView txt_category_name;

        LeftItemLayoutBinding l_Binding;

        CategoryListModel.CategoryList _category;


        public CategoryListModel getCategory() {
            return category;
        }

        public void setCategory(CategoryListModel category) {
            this.category = category;
        }

        public MyViewHolder(final Activity activity, @NonNull View itemView) {
            super(itemView);


            l_Binding = DataBindingUtil.bind(itemView);


            l_Binding.ivProductThumb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Intent intent = new Intent(context, CategoryDetailActivity.class)
                            .putExtra(CategoryDetailActivity.Category, _category);

                    Bundle b = ActivityOptionsCompat
                            .makeSceneTransitionAnimation(activity, l_Binding.ivProductThumb, "image").toBundle();
                    intent.putExtra("categoryid", _category.getCategoryID());
                    context.startActivity(intent, b);

                }
            });


            //  iv_product_thumb = (ImageView) itemView.findViewById(R.id.iv_product_thumb);
            // txt_category_name = (TextView) itemView.findViewById(R.id.txt_category_name);


        }

        public CategoryListModel.CategoryList get_category() {
            return _category;
        }

        public void set_category(CategoryListModel.CategoryList _category) {
            this._category = _category;
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (position % 2 == 0) {
            return 0;
        } else {
            return 1;
        }

    }
}
