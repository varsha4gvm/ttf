package com.paul.ttf.CustomControl.DatePicker;

/** The range state of a cell for {@link MonthCellDescriptor} and {@link CalendarCellView}*/
public enum RangeState {
    NONE, FIRST, MIDDLE, LAST
}
