package com.paul.ttf.UI;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.paul.ttf.CustomControl.MyWebViewClient;
import com.paul.ttf.R;
import com.paul.ttf.Utility.Constants;

public class WebviewActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signup_screen);

        init();
    }

    private void init() {
        WebView webView = (WebView) findViewById(R.id.webview);
        final ProgressBar progress_bar = (ProgressBar) findViewById(R.id.progress);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                //Make the bar disappear after URL is loaded, and changes string to Loading...
                // setTitle("Loading...");
                //setProgress(progress * 100); //Make the bar disappear after URL is loaded

                progress_bar.setProgress(progress);

                // Return the paul name after finish loading
                if (progress == 100) {
                    progress_bar.setVisibility(View.GONE);
                    setTitle(R.string.app_name);
                }
            }
        });

        MyWebViewClient webViewClient = new MyWebViewClient(this);
        webView.setWebViewClient(webViewClient);

        webView.loadUrl(Constants.GET_SIGNUP_URL);
    }
}
