package com.paul.ttf.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AllCategoryVideoModel {


    @SerializedName("code")
    public Integer code;

    @SerializedName("message")
    public String message;

    @SerializedName("List")
    public ArrayList<List> List;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AllCategoryVideoModel.List> getList() {
        return List;
    }

    public void setList(ArrayList<AllCategoryVideoModel.List> list) {
        List = list;
    }

    public static class List {

        @SerializedName("ID")
        public int ID;


        @SerializedName("Name")
        public String Name;

        @SerializedName("VideoList")
        public ArrayList<VideoList> VideoList;


        public ArrayList<AllCategoryVideoModel.List.VideoList> getVideoList() {
            return VideoList;
        }

        public void setVideoList(ArrayList<AllCategoryVideoModel.List.VideoList> videoList) {
            VideoList = videoList;
        }

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public static class VideoList {


            @SerializedName("VideoID")
            public int VideoID;

            @SerializedName("VideoName")
            public String VideoName;

            public int getVideoID() {
                return VideoID;
            }

            public void setVideoID(int videoID) {
                VideoID = videoID;
            }

            public String getVideoName() {
                return VideoName;
            }

            public void setVideoName(String videoName) {
                VideoName = videoName;
            }
        }


    }

}
