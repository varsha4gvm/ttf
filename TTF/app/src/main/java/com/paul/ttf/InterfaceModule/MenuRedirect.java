package com.paul.ttf.InterfaceModule;


import com.paul.ttf.Models.MenuModel;

public interface MenuRedirect {

    public void MenuRedirectEvent(MenuModel object);
}
