package com.paul.ttf.Extract;

public class ExtractorException extends Exception
{
	public ExtractorException(String msg){
		super(msg);
	}
}
