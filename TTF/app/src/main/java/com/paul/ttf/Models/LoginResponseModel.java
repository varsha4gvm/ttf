package com.paul.ttf.Models;

import com.google.gson.annotations.SerializedName;

public class LoginResponseModel {

    @SerializedName("code")
    public Integer code;

    @SerializedName("message")
    public String message;

    @SerializedName("LoginInfo")
    public LoginInfo LoginInfo;


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginResponseModel.LoginInfo getLoginInfo() {
        return LoginInfo;
    }

    public void setLoginInfo(LoginResponseModel.LoginInfo loginInfo) {
        LoginInfo = loginInfo;
    }

    public class LoginInfo {


        @SerializedName("Token")
        public String Token;

        @SerializedName("memberid")
        public Integer memberid;

        @SerializedName("firstname")
        public String firstname;

        @SerializedName("surname")
        public String surname;

        @SerializedName("email")
        public String email;

        @SerializedName("gender")
        public String gender;

        @SerializedName("joindate")
        public String joindate;

        @SerializedName("memberphoto")
        public String memberphoto;


        public Integer getMemberid() {
            return memberid;
        }

        public void setMemberid(Integer memberid) {
            this.memberid = memberid;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getJoindate() {
            return joindate;
        }

        public void setJoindate(String joindate) {
            this.joindate = joindate;
        }

        public String getMemberphoto() {
            return memberphoto;
        }

        public void setMemberphoto(String memberphoto) {
            this.memberphoto = memberphoto;
        }

        public String getToken() {
            return Token;
        }

        public void setToken(String token) {
            Token = token;
        }
    }
}
