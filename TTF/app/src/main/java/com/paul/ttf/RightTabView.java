package com.paul.ttf;

import android.content.Context;

public class RightTabView extends TabView {

    @Override
    protected float[] getCornerRadii(int roundRadius) {
        return new float[]{0, 0, roundRadius, roundRadius, roundRadius, roundRadius, 0, 0};
    }

    public RightTabView(Context context, int layout) {
        super(context, layout);
    }
}
