package com.paul.ttf.CustomControl.DatePicker;

/** Adapter used to provide a layout for {@link CalendarCellView}.*/
public interface DayViewAdapter {
  void makeCellView(CalendarCellView parent);
}
