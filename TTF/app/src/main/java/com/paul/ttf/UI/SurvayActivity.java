package com.paul.ttf.UI;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AAChartModel;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AAChartSymbolStyleType;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AAChartSymbolType;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AAChartType;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AAChartView;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AAGradientColor;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AALinearGradientDirection;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AASeriesElement;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.paul.ttf.APIThread.APIClient;
import com.paul.ttf.APIThread.APIInterface;
import com.paul.ttf.Adapter.CustomAdapter;
import com.paul.ttf.Adapter.CustomVideoAdapter;
import com.paul.ttf.CustomControl.DatePicker.CalendarPickerView;
import com.paul.ttf.Entry;
import com.paul.ttf.Highlight;
import com.paul.ttf.LoadingView;
import com.paul.ttf.Models.AllCategoryVideoModel;
import com.paul.ttf.Models.CategoryVideoModel;
import com.paul.ttf.Models.UsereviewRequestModel;
import com.paul.ttf.Models.UsereviewResponseModel;
import com.paul.ttf.OnChartValueSelectedListener;
import com.paul.ttf.R;
import com.paul.ttf.Utility.CommonFunction;
import com.paul.ttf.Utility.Constants;
import com.paul.ttf.Utility.FileLog;
import com.paul.ttf.Utility.SharedPreferencesUtility;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class SurvayActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, View.OnTouchListener,
        OnChartValueSelectedListener {


    SharedPreferencesUtility _shared_prefrance;
    private final CompositeDisposable disposables = new CompositeDisposable();

    CoordinatorLayout coordinate_parent;
    private LoadingView mLoadingView;
    Spinner spinner_category, spinner_video;
    EditText edit_fromdate;
    Button btn_login;
    //CombinedChart chartdata;
    AAChartView chartdata;

    String From_date = "", To_date = "";
    AllCategoryVideoModel.List _category_obj;
    CategoryVideoModel.Video_List _video_obj;

    Dialog mDialog;

    int counts = 30, range = 180;

    ArrayList<CategoryVideoModel.Video_List> productList;
    int Categoryid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_survay_screen);
        setUpNavigationView();
        _shared_prefrance = new SharedPreferencesUtility(SurvayActivity.this);

        productList = new ArrayList<>();
        String _videostr = getIntent().getStringExtra("videolist");
        Categoryid = getIntent().getIntExtra("Categoryid", 0);
        Gson _gson = new Gson();
        Type listOfdoctorType = new TypeToken<List<CategoryVideoModel.Video_List>>() {
        }.getType();
        productList = _gson.fromJson(_videostr, listOfdoctorType);

        init();
    }


    private void setUpNavigationView() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // perform whatever you want on back arrow click
                onBackPressed();
            }
        });
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu


         toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
    }

    private void init() {

        coordinate_parent = findViewById(R.id.coordinate_parent);
        spinner_category = findViewById(R.id.spinner_category);
        spinner_video = findViewById(R.id.spinner_video);
        edit_fromdate = findViewById(R.id.edit_fromdate);
        chartdata = (AAChartView) findViewById(R.id.chart1);

        // create marker to display box when values are selected

        btn_login = findViewById(R.id.btn_login);

        btn_login.setOnClickListener(this);
        edit_fromdate.setOnTouchListener(this);
        spinner_category.setOnItemSelectedListener(this);

        mLoadingView = (LoadingView) findViewById(R.id.loading_view_repeat);
        ConfigureProgress();


        if (CommonFunction.isInternetAvailable(SurvayActivity.this)) {

            FillVideoSpinner(productList);
        } else {
            Snackbar snackbar = Snackbar
                    .make(coordinate_parent, getString(R.string.error_selection), Snackbar.LENGTH_LONG);

            snackbar.setActionTextColor(Color.RED);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        }

        InitConfigure();
    }

    private void InitConfigure() {

        final String TAG = "dialogSorryCall_dialog";
        TextView tvMessage, tvYes, tvNo;
        LayoutInflater inflater = LayoutInflater.from(SurvayActivity.this);
        mDialog = new Dialog(SurvayActivity.this,
                android.R.style.Widget_Material);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);

        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.dialog_logout, null);

        final Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 10);

        final Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -10);


        final CalendarPickerView calendar = dialoglayout.findViewById(R.id.calendar_view);
        Button get_selected_dates = dialoglayout.findViewById(R.id.get_selected_dates);
        ArrayList<Integer> list = new ArrayList<>();
        // list.add(2);


        calendar.deactivateDates(list);
        ArrayList<Date> arrayList = new ArrayList<>();
        try {
            SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");

            String strdate = "22-4-2019";
            String strdate2 = "26-4-2019";

            Date newdate = dateformat.parse(strdate);
            Date newdate2 = dateformat.parse(strdate2);
            arrayList.add(newdate);
            arrayList.add(newdate2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        calendar.init(lastYear.getTime(), nextYear.getTime(), new SimpleDateFormat("MMMM, yyyy", Locale.getDefault())) //
                .inMode(CalendarPickerView.SelectionMode.RANGE); //
                /*.withDeactivateDates(list)
                .withSubTitles(getSubTitles())
                .withHighlightedDates(arrayList);*/

        calendar.scrollToDate(new Date());
        mDialog.setContentView(dialoglayout);
        mDialog.setCanceledOnTouchOutside(false);

        get_selected_dates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                for (int i = 0; i < calendar.getSelectedDates().size(); i++) {
                    FileLog.e("TAGGG", "okhttp check result > " + calendar.getSelectedDates().get(i));
                    if (i == 0) {
                        From_date = DateFormat.format("yyyy-MM-dd", new Date(calendar.getSelectedDates().get(i).getTime())).toString();
                        FileLog.e("TAGGG", "okhttp check result First> " + calendar.getSelectedDates().get(i).getTime() + " Convert " + From_date);
                        edit_fromdate.setText(From_date);
                    }
                    if (i == (calendar.getSelectedDates().size() - 1)) {
                        To_date = DateFormat.format("yyyy-MM-dd", new Date(calendar.getSelectedDates().get(i).getTime())).toString();
                        FileLog.e("TAGGG", "okhttp check result Last> " + calendar.getSelectedDates().get(i).getTime() + " Convert " + To_date);
                        edit_fromdate.setText(Html.fromHtml("<b>From </b> " + edit_fromdate.getText().toString() + " <b>To</b>  " + To_date));
                    }
                }
            }
        });


        Calendar _current = Calendar.getInstance();
        To_date = DateFormat.format("yyyy-MM-dd", _current.getTime()).toString();
        From_date = DateFormat.format("yyyy-MM-dd", _current.getTime()).toString();

        edit_fromdate.setText(Html.fromHtml("<b>From </b> " + From_date + " <b>To</b>  " + To_date));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


    private void ConfigureProgress() {


        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        int marvel_1 = R.drawable.marvel_1_lollipop;
        int marvel_2 = R.drawable.marvel_2_lollipop;
        int marvel_3 = R.drawable.marvel_3_lollipop;
        int marvel_4 = R.drawable.marvel_4_lollipop;
        mLoadingView.addAnimation(Color.parseColor("#FFD200"), marvel_1,
                LoadingView.FROM_LEFT);
        mLoadingView.addAnimation(Color.parseColor("#2F5DA9"), marvel_2,
                LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#FF4218"), marvel_3,
                LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);

        mLoadingView.addListener(new LoadingView.LoadingListener() {
            @Override
            public void onAnimationStart(int currentItemPosition) {

            }

            @Override
            public void onAnimationRepeat(int nextItemPosition) {

            }

            @Override
            public void onAnimationEnd(int nextItemPosition) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_login) {
            if (_video_obj != null) {
                if (IsValid()) {
                    GetUserSurvaydata();
                }

            } else {
                Snackbar snackbar = Snackbar
                        .make(coordinate_parent, getString(R.string.error_selection), Snackbar.LENGTH_LONG);

                snackbar.setActionTextColor(Color.RED);
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        }
    }


    private Observable<? extends Long> getObservable() {
        return Observable.interval(0, 500, TimeUnit.MILLISECONDS);
    }

    private void doProgressStart() {
        mLoadingView.setVisibility(View.VISIBLE);
        disposables.add(getObservable()
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getObserver()));
    }

    private DisposableObserver<Long> getObserver() {
        return new DisposableObserver<Long>() {

            @Override
            public void onNext(Long value) {
                //mLoadingView.startAnimation();
                Log.d("TAGGG", " onNext : value : " + value);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
               /* textView.append(" onComplete");
                textView.append(AppConstant.LINE_SEPARATOR);
                Log.d(TAG, " onComplete");*/
            }
        };
    }


    private void DismissProgress() {
        FileLog.e("TAGGG", "Number converter String Call 2");
        mLoadingView.setVisibility(View.GONE);
        disposables.clear(); // clearing it : do not emit after destroy
    }

  /*  private void GetCategoryVideodata() {

        APIInterface gerritAPI = APIClient.getClient().create(APIInterface.class);
        String Category_uri = Constants.BASE_URL + Constants.GET_ALL_CATEGORY_VIDEO_URL;
        Observable<AllCategoryVideoModel> profileResponse = gerritAPI.getAllCategoryVideoList(Category_uri);
        doProgressStart();
        profileResponse.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<AllCategoryVideoModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(AllCategoryVideoModel categoryListModel) {


                if (categoryListModel.getCode() == 200) {
                    FileLog.e("TAGGG", "Check Print converter String  here onNext>> " + categoryListModel.getList().size());
                    FillSpinner(categoryListModel);
                } else {
                    Snackbar snackbar = Snackbar
                            .make(coordinate_parent, categoryListModel.getMessage(), Snackbar.LENGTH_LONG);

                    snackbar.setActionTextColor(Color.RED);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                }

            }

            @Override
            public void onError(Throwable e) {
                DismissProgress();
            }

            @Override
            public void onComplete() {
                DismissProgress();
                FileLog.e("TAGGG", "Number converter String  here onComplete>> ");
            }
        });

    }*/

    private void FillSpinner(AllCategoryVideoModel categoryListModel) {

        CustomAdapter adapter = new CustomAdapter(SurvayActivity.this,
                R.layout.rowitem_layout, R.id.txt_label, categoryListModel.getList());
        spinner_category.setAdapter(adapter);

    }


    private void FillVideoSpinner(ArrayList<CategoryVideoModel.Video_List> videoListModel) {

        CustomVideoAdapter adapter = new CustomVideoAdapter(SurvayActivity.this,
                R.layout.rowitem_layout, R.id.txt_label, videoListModel);
        spinner_video.setAdapter(adapter);
        spinner_video.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    _video_obj = (CategoryVideoModel.Video_List) view.getTag();
                } catch (Exception ex) {

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        // if (view.getId() == R.id.spinner_category) {
        _category_obj = (AllCategoryVideoModel.List) view.getTag();

        /*} else if (view.getId() == R.id.spinner_video) {
            AllCategoryVideoModel.List.VideoList _video_obj = (AllCategoryVideoModel.List.VideoList) view.getTag();
        }*/
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent motionEvent) {

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // touch down code
                showDatePicker();

                break;

            case MotionEvent.ACTION_MOVE:
                // touch move code
                break;

            case MotionEvent.ACTION_UP:
                // touch up code
                break;
        }

        return true;
    }

    public void showDatePicker() {

        mDialog.show();
    }

    private final RectF onValueSelectedRectF = new RectF();

    @Override
    public void onValueSelected(Entry e, Highlight h) {

/*        Log.i("Entry selected", e.toString());
        Log.i("LOW HIGH", "low: " + chartdata.getLowestVisibleX() + ", high: " + chartdata.getHighestVisibleX());
        Log.i("MIN MAX", "xMin: " + chartdata.getXChartMin() + ", xMax: " + chartdata.getXChartMax() + ", yMin: " + chartdata.getYChartMin() + ", yMax: " + chartdata.getYChartMax());*/
    }

    @Override
    public void onNothingSelected() {

    }


    private void GetUserSurvaydata() {

        //APIInterface gerritAPI = APIClient.getClient().create(APIInterface.class);

        APIInterface gerritAPI;
        if (!_shared_prefrance.getToken().equals("")) {
            gerritAPI = APIClient.getClientHeader(_shared_prefrance.getToken()).create(APIInterface.class);
        } else {
            gerritAPI = APIClient.getClientHeader(Constants.Guest_Token).create(APIInterface.class);
        }

        SharedPreferencesUtility _sharePrefrance = new SharedPreferencesUtility(ApplicationClass.applicationContext);

        UsereviewRequestModel _model = new UsereviewRequestModel();
        _model.setCategoryID(Categoryid);
        _model.setMemberID(_sharePrefrance.getMemberid());
        _model.setVideoID(_video_obj.getVideoID());
        _model.setFromDate(From_date);
        _model.setToDate(To_date);


        Observable<UsereviewResponseModel> profileResponse = gerritAPI.GetUserSurvay(_model);
        doProgressStart();
        profileResponse.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<UsereviewResponseModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(UsereviewResponseModel categoryListModel) {


                if (categoryListModel.getCode() == 200) {
                    FileLog.e("TAGGG", "Number converter String  here onNext>> " + categoryListModel.getLstRecords().size());
                    //setData(categoryListModel);
                    SimpleDateFormat fromformat = new SimpleDateFormat("MMM-dd-yyyy");
                    SimpleDateFormat toformat = new SimpleDateFormat("dd-MMM");
                    for (int i = 0; i < categoryListModel.getLstRecords().size(); i++) {
                        try {
                            Date date = fromformat.parse(categoryListModel.getLstRecords().get(i).getDate());
                            categoryListModel.getLstRecords().get(i).setDate(toformat.format(date));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                    LoadBarChart(categoryListModel);

                    chartdata.invalidate();
                } else if (categoryListModel.getCode() == 401) {

                    ShowAuthError(categoryListModel.getMessage(), getString(R.string.error_auth));

                } else {
                    Snackbar snackbar = Snackbar
                            .make(coordinate_parent, categoryListModel.getMessage(), Snackbar.LENGTH_LONG);

                    snackbar.setActionTextColor(Color.RED);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                }

            }

            @Override
            public void onError(Throwable e) {
                DismissProgress();
            }

            @Override
            public void onComplete() {
                DismissProgress();
                FileLog.e("TAGGG", "Number converter String  here onComplete>> ");
            }
        });

    }

    private void LoadBarChart(final UsereviewResponseModel categoryListModel) {


        AAChartModel aaChartModel = configureTheAAChartModel(categoryListModel);
        chartdata.aa_drawChartWithChartModel(aaChartModel);

        /*chartdata.getDescription().setEnabled(false);
        chartdata.setBackgroundColor(Color.WHITE);
        chartdata.setDrawGridBackground(false);
        chartdata.setDrawBarShadow(false);
        chartdata.setHighlightFullBarEnabled(false);
        chartdata.setBackgroundColor(getResources().getColor(R.color.chart_background));

        // draw bars behind lines
        chartdata.setDrawOrder(new CombinedChart.DrawOrder[]{
                CombinedChart.DrawOrder.LINE
        });

       *//* Legend l = chartdata.getLegend();
        l.setWordWrapEnabled(true);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);*//*

        YAxis rightAxis = chartdata.getAxisRight();
        rightAxis.setDrawGridLines(true);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        rightAxis.setEnabled(false);

        YAxis leftAxis = chartdata.getAxisLeft();
        leftAxis.setDrawGridLines(true);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        XAxis xAxis = chartdata.getXAxis();
        xAxis.setLabelRotationAngle(-25);
        xAxis.setDrawGridLines(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0f);
        Log.e("TAGGG", "Check Value here > " + (categoryListModel.getLstRecords().size() < 15));
        if (categoryListModel.getLstRecords().size() < 15) {
            xAxis.setGranularity(1f);
        } else {
            xAxis.setGranularity(1f);
        }

        //xAxis.setLabelsToSkip(5);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                Log.e("TAGGG", "Check Value here > " + value);
                String Xvalue = "";
                for (int index = 0; index < categoryListModel.getLstRecords().size(); index++) {
                    //for (int index = 0; index < counts; index++) {
                    if ((int) value == index) {
                        //Xvalue = SurvayActivity.this.getFormattedValue(index);
                        Xvalue = categoryListModel.getLstRecords().get(index).getDate();
                        break;
                    }
                }
                return Xvalue;
            }
        });

        CombinedData data = new CombinedData();

        data.setData(generateLineData(categoryListModel));

        //data.setValueTypeface(tfLight);

        xAxis.setAxisMaximum(data.getXMax());

        chartdata.getLegend().setEnabled(false);
        chartdata.setData(data);
        chartdata.invalidate();*/
    }

   /* private LineData generateLineData(UsereviewResponseModel categoryListModel) {

        LineData d = new LineData();

        ArrayList<Entry> entries = new ArrayList<>();

        for (int index = 0; index < categoryListModel.getLstRecords().size(); index++)
            entries.add(new Entry(index, categoryListModel.getLstRecords().get(index).getPopupValue()));

        *//*for (int index = 0; index < counts; index++) {
            float val = (float) (Math.random() * range) - 30;
            entries.add(new Entry(index, val));
        }*//*

        LineDataSet set = new LineDataSet(entries, "");
        set.setColor(getResources().getColor(R.color.black));
        set.setLineWidth(3f);
        set.setCircleColor(getResources().getColor(R.color.black));
        set.setCircleRadius(5f);
        set.setFillColor(getResources().getColor(R.color.black));
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setDrawValues(false);
        set.setValueTextSize(0f);
        set.setValueTextColor(getResources().getColor(R.color.black));

        set.setDrawFilled(true);
        if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(SurvayActivity.this, R.drawable.fill_chart);
            set.setFillDrawable(drawable);
        } else {
            set.setFillColor(R.color.black_transp);
        }


        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setDrawCircles(true);
        d.addDataSet(set);


        return d;
    }

    public String getFormattedValue(int value) {

        int days = (int) value;

        int year = determineYear(days);

        int month = determineMonth(days);
        String monthName = mMonths[month % mMonths.length];
        String yearName = String.valueOf(year);

        if (chartdata.getVisibleXRange() > 30 * 6) {

            return monthName + " " + yearName;
        } else {

            int dayOfMonth = determineDayOfMonth(days, month + 12 * (year - 2016));

            String appendix = "th";

            switch (dayOfMonth) {
                case 1:
                    appendix = "st";
                    break;
                case 2:
                    appendix = "nd";
                    break;
                case 3:
                    appendix = "rd";
                    break;
                case 21:
                    appendix = "st";
                    break;
                case 22:
                    appendix = "nd";
                    break;
                case 23:
                    appendix = "rd";
                    break;
                case 31:
                    appendix = "st";
                    break;
            }

            return dayOfMonth == 0 ? "" : dayOfMonth + appendix + " " + monthName;
        }
    }*/


        public AAChartModel configureTheAAChartModel(UsereviewResponseModel categoryListModel) {

        String[] xValue = new String[categoryListModel.getLstRecords().size()];
        Object[] yValue = new Object[categoryListModel.getLstRecords().size()];
        for (int i = 0; i < categoryListModel.getLstRecords().size(); i++) {
            xValue[i] = categoryListModel.getLstRecords().get(i).getDate();
            yValue[i] = categoryListModel.getLstRecords().get(i).getPopupValue();
        }

        Object[] stopsArr = new Object[]{new Object[]{0, "rgba(00,00,00,0.5)"}, new Object[]{1, "rgba(00,00,00,0.5)"}};
        HashMap linearGradientColor = AAGradientColor.INSTANCE.linearGradient(AALinearGradientDirection.ToBottom, stopsArr);
        return (new AAChartModel()).chartType(AAChartType.Areaspline).title("").subtitle("").categories(xValue).yAxisTitle("").axesTextColor("rgba(00,00,00,0.5)").markerRadius(8.0F).markerSymbolStyle(AAChartSymbolStyleType.Normal).markerSymbol(AAChartSymbolType.Circle).yAxisLineWidth(1F).yAxisGridLineWidth(1F).legendEnabled(true).series(new AASeriesElement[]{(new AASeriesElement()).name("TTF").lineWidth(2F).color("rgba(00, 00, 00, 1)").fillColor(linearGradientColor).data(yValue)});
    }


    private boolean IsValid() {
        boolean isvalid = true;
        if (edit_fromdate.getText().toString().trim().equals("")) {

            Snackbar snackbar = Snackbar
                    .make(coordinate_parent, getString(R.string.text_from_hint), Snackbar.LENGTH_LONG);

            snackbar.setActionTextColor(Color.RED);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();

            isvalid = false;
        }

        return isvalid;
    }

    public boolean ShowAuthError(String msg_title, String msg_detail) {


        TextView title = new TextView(this);
        title.setText(msg_title);
        title.setPadding(15, 15, 15, 10);
        title.setGravity(Gravity.LEFT);
// title.setTextColor(getResources().getColor(R.color.greenBG));
        title.setTextSize(18);

        TextView msg = new TextView(this);
        msg.setText(msg_detail);
        msg.setPadding(15, 10, 15, 10);
        msg.setGravity(Gravity.LEFT);
        msg.setTextSize(16);

        DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) { //Delete for me

                    SharedPreferencesUtility _sharePreference = new SharedPreferencesUtility(SurvayActivity.this);
                    _sharePreference.ClearPrefrance();


                    Intent intent = new Intent(SurvayActivity.this, SplashActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                    dialog.dismiss();
                }

            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCustomTitle(title);
        builder.setView(msg);
        //builder.setCancelable(true);
        builder.setPositiveButton(getString(R.string.txt_logout), onClick);
        //builder.setNegativeButton(getString(R.string.txt_delete_cancel), onClick);


        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        return true;
    }

}

