package com.paul.ttf.Models;

import com.google.gson.annotations.SerializedName;

public class UsereviewRequestModel {

    @SerializedName("MemberID")
    public Integer MemberID;

    @SerializedName("CategoryID")
    public Integer CategoryID;

    @SerializedName("VideoID")
    public Integer VideoID;

    @SerializedName("FromDate")
    public String FromDate;


    @SerializedName("ToDate")
    public String ToDate;


    public Integer getMemberID() {
        return MemberID;
    }

    public void setMemberID(Integer memberID) {
        MemberID = memberID;
    }

    public Integer getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(Integer categoryID) {
        CategoryID = categoryID;
    }

    public Integer getVideoID() {
        return VideoID;
    }

    public void setVideoID(Integer videoID) {
        VideoID = videoID;
    }

    public String getFromDate() {
        return FromDate;
    }

    public void setFromDate(String fromDate) {
        FromDate = fromDate;
    }

    public String getToDate() {
        return ToDate;
    }

    public void setToDate(String toDate) {
        ToDate = toDate;
    }
}