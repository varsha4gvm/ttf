package com.paul.ttf.InterfaceModule;

import com.paul.ttf.Models.CategoryVideoModel;

public interface CategoryVideoInterface {
    public void onSelectCategoryItem(CategoryVideoModel.Video_List position);

    public void onShowToastMessage(String Message);

    public void onShowProgress();

    public void onHideProgress();

    public void onSelectOptionItem(CategoryVideoModel.Video_List position);


    public void onHideDialog();

    public void onExitActivity();


    public void Setfullscreen(boolean isFull);

    public void PauseVideoHere();

    public void onShowView();

    public void onHideView();

}