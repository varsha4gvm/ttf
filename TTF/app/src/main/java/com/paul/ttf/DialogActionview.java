package com.paul.ttf;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AAChartModel;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AAChartSymbolStyleType;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AAChartSymbolType;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AAChartType;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AAChartView;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AAGradientColor;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AALinearGradientDirection;
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartConfiger.AASeriesElement;
import com.paul.ttf.APIThread.APIClient;
import com.paul.ttf.APIThread.APIInterface;
import com.paul.ttf.Adapter.DialogPopupAdapter;
import com.paul.ttf.InterfaceModule.ButtonInterface;
import com.paul.ttf.InterfaceModule.CategoryVideoInterface;
import com.paul.ttf.InterfaceModule.DialogPreviewCallBack;
import com.paul.ttf.Models.CategoryVideoModel;
import com.paul.ttf.Models.SurvayRequestModel;
import com.paul.ttf.Models.SurvayResponseModel;
import com.paul.ttf.Models.UsereviewRequestModel;
import com.paul.ttf.Models.UsereviewResponseModel;
import com.paul.ttf.UI.ApplicationClass;
import com.paul.ttf.UI.SplashActivity;
import com.paul.ttf.Utility.Constants;
import com.paul.ttf.Utility.FileLog;
import com.paul.ttf.Utility.SharedPreferencesUtility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class DialogActionview extends FrameLayout implements ButtonInterface, View.OnClickListener {

    TextView txt_dialog_title, txt_dialog_dismiss;
    EditText edit_input_value;
    RecyclerView recycleview_button;
    Context _context;
    SharedPreferencesUtility _shared_prefrance;
    CategoryVideoInterface _callbackInterface;
    CategoryVideoModel.Video_List.settimepopup _survay_Detail;
    RelativeLayout ll_main, ll_chart;
    ImageView img_button;
    CardView card_view;
    Boolean IsEntryData = true;
    String VideoName;
    Integer VideoID = 0, CategoryID = 0, TestQueryID = 0;
    public boolean isClick = false;

    AAChartView chartdata;
    DialogPreviewCallBack _callback;
    boolean isMember;


    public DialogActionview(@NonNull Context context) {
        super(context);
        this._context = context;
        _shared_prefrance = new SharedPreferencesUtility(_context);
    }

    public DialogActionview(Context context, AttributeSet attrs) {
        super(context, attrs);
        this._context = context;
        _shared_prefrance = new SharedPreferencesUtility(_context);
        init();
    }

    public DialogActionview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this._context = context;
        _shared_prefrance = new SharedPreferencesUtility(_context);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.alartdialog_layout, this);
        ll_main = (RelativeLayout) findViewById(R.id.ll_main);
        ll_chart = (RelativeLayout) findViewById(R.id.ll_chart);
        img_button = (ImageView) findViewById(R.id.img_button);
        chartdata = (AAChartView) findViewById(R.id.chartdata);

        txt_dialog_title = (TextView) findViewById(R.id.txt_dialog_title);
        txt_dialog_dismiss = (TextView) findViewById(R.id.txt_dialog_dismiss);
        edit_input_value = (EditText) findViewById(R.id.edit_input_value);
        recycleview_button = (RecyclerView) findViewById(R.id.recycleview_button);
        card_view = (CardView) findViewById(R.id.card_view);

        img_button.setOnClickListener(this);
    }

    public void UpdateFillTitle(CategoryVideoModel.Video_List.settimepopup _Objectdata, CategoryVideoInterface _callbackInterface) {
        this._callbackInterface = _callbackInterface;
        txt_dialog_title.setText(_Objectdata.getPopUp_Text());
        if (_Objectdata.isInput()) {
            edit_input_value.setVisibility(VISIBLE);
        } else {
            edit_input_value.setVisibility(GONE);
        }
    }

    public void NONUpdateFillTitle() {
        txt_dialog_title.setText(_context.getResources().getString(R.string.txt_valication));
        final LinearLayout linear_dialog = findViewById(R.id.linear_dialog);
        Button btn_click = findViewById(R.id.btn_click);
        Button btn_cancel = findViewById(R.id.btn_cancel);

        linear_dialog.setVisibility(VISIBLE);

        ll_main.setBackgroundColor(getResources().getColor(R.color.lart_bg_color));
        edit_input_value.setVisibility(GONE);
        recycleview_button.setVisibility(GONE);
        img_button.setVisibility(GONE);


        btn_click.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                _callbackInterface.onExitActivity();
            }
        });

        btn_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                _callback.ForceHide(isMember);
            }
        });
    }

    public void UpdateFillButton(CategoryVideoModel.Video_List.settimepopup _Objectdata, Integer VideoID, Integer CategoryID, Integer TestQueryID, boolean IsEntryData, String VideoName, boolean isMember) {
        isClick = false;
        _survay_Detail = _Objectdata;
        this.isMember = isMember;
        this.VideoID = VideoID;
        this.VideoName = VideoName;
        this.CategoryID = CategoryID;
        this.TestQueryID = TestQueryID;
        //card_view.setAlpha((_Objectdata.getPopUp_Opacity() / 100));
        FileLog.e("TAGGG", "Check pass data here >> " + IsEntryData + " > " + isMember);

        if (IsEntryData) {
            ll_main.setVisibility(VISIBLE);
            ll_chart.setVisibility(GONE);
            fillList(_Objectdata, VideoID, CategoryID);
        } else {
            if (isMember) {
                ll_main.setVisibility(GONE);

                AAChartModel aaChartModel = new AAChartModel();
                chartdata.aa_drawChartWithChartModel(aaChartModel);
                chartdata.invalidate();

                GetUserSurvaydata();
            } else {
                ll_main.setVisibility(VISIBLE);
                ll_chart.setVisibility(GONE);
            }


        }


    }

    public void ShowDismissText() {
        txt_dialog_dismiss.setVisibility(VISIBLE);
    }

    public void UpdateDismissText(String value) {
        txt_dialog_dismiss.setText(value);
    }

    private void fillList(CategoryVideoModel.Video_List.settimepopup _Objectdata, Integer VideoID, Integer CategoryID) {
        DialogPopupAdapter adapter = new DialogPopupAdapter(_Objectdata.getButtonList(), _context, this);
        recycleview_button.setLayoutManager(new LinearLayoutManager(_context, LinearLayoutManager.HORIZONTAL, true));
        recycleview_button.setAdapter(adapter);
    }


    @Override
    public void onSelectButtonItem(CategoryVideoModel.Video_List.settimepopup.ButtonList _obj) {
        if (edit_input_value.getVisibility() == VISIBLE) {
            TakeSurvay(false);
        } else {
            _callbackInterface.onHideDialog();
        }

    }

    public void TakeSurvay(boolean isAutoDismiss) {

        boolean methodCall = true;
        if (isAutoDismiss && edit_input_value.getText().toString().equals("")) {
            methodCall = false;
        }
        if (methodCall) {
            APIInterface gerritAPI;
            if (!_shared_prefrance.getToken().equals("")) {
                gerritAPI = APIClient.getClientHeader(_shared_prefrance.getToken()).create(APIInterface.class);
            } else {
                gerritAPI = APIClient.getClientHeader(Constants.Guest_Token).create(APIInterface.class);
            }

            //  APIInterface gerritAPI = APIClient.getClient().create(APIInterface.class);
            //String Category_uri = Constants.BASE_URL + Constants.GET_CATEGORY_VIDEO_URL + "?CategoryID=";

            SurvayRequestModel _model = new SurvayRequestModel();
            SharedPreferencesUtility _sharePrefrance = new SharedPreferencesUtility(ApplicationClass.applicationContext);
            _model.setMemberID(_sharePrefrance.getMemberid());
            _model.setCategoryID(CategoryID);
            _model.setVideoID(VideoID);
            _model.setTestQueryID(TestQueryID);
            _model.setPopupID(_survay_Detail.getTimelineID());
            if (edit_input_value.getVisibility() == VISIBLE) {
                try {
                    _model.setPopValue(Double.parseDouble(edit_input_value.getText().toString()));
                } catch (Exception ex) {
                    _model.setPopValue(0d);
                }

            } else {
                _model.setPopValue(0d);
            }


            Observable<SurvayResponseModel> profileResponse = gerritAPI.TakeSurvay(_model);
            _callbackInterface.onShowProgress();
            profileResponse.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<SurvayResponseModel>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(SurvayResponseModel categoryListModel) {
                    FileLog.e("TAGGG", "Number converter String  here onNext>> " + categoryListModel.getMessage());

                    if (categoryListModel.getCode() == 200) {
                        _callbackInterface.onShowToastMessage(_context.getResources().getString(R.string.text_survay));
                        _callbackInterface.onHideDialog();
                    } else if (categoryListModel.getCode() == 401) {

                        ShowAuthError(categoryListModel.getMessage(), _context.getString(R.string.error_auth));

                    } else {
                        _callbackInterface.onShowToastMessage(categoryListModel.getMessage());
                    }
                }

                @Override
                public void onError(Throwable e) {
                    _callbackInterface.onHideProgress();
                }

                @Override
                public void onComplete() {
                    _callbackInterface.onHideProgress();
                    FileLog.e("TAGGG", "Number converter String  here onComplete>> ");
                }
            });
        }
    }

    public void ClearEditText() {
        edit_input_value.setText("");
        HideKeyboardText();
    }

    public void HideKeyboardText() {
        InputMethodManager inputManager = (InputMethodManager) _context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(edit_input_value.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public void UpdateCallbackObject(CategoryVideoInterface _callbackInterface, boolean IsEntryData, DialogPreviewCallBack _callback, boolean isMember) {
        this._callbackInterface = _callbackInterface;
        this.IsEntryData = IsEntryData;
        this._callback = _callback;
        this.isMember = isMember;

        FileLog.e("TAGGG", "Check pass data here <C> " + (IsEntryData && isMember));
        if (IsEntryData) {
            ll_main.setVisibility(VISIBLE);
            ll_chart.setVisibility(GONE);
        } else {
            if (isMember) {
                ll_main.setVisibility(GONE);
                ll_chart.setVisibility(VISIBLE);
            } else {
                ll_main.setVisibility(VISIBLE);
                ll_chart.setVisibility(GONE);
            }
        }
    }


    private void GetUserSurvaydata() {

        Calendar calender = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String toDate = df.format(calender.getTime());

        calender.add(Calendar.MONTH, -1);
        String formDate = df.format(calender.getTime());


        APIInterface gerritAPI;
        if (!_shared_prefrance.getToken().equals("")) {
            gerritAPI = APIClient.getClientHeader(_shared_prefrance.getToken()).create(APIInterface.class);
        } else {
            gerritAPI = APIClient.getClientHeader(Constants.Guest_Token).create(APIInterface.class);
        }

        SharedPreferencesUtility _sharePrefrance = new SharedPreferencesUtility(ApplicationClass.applicationContext);

        UsereviewRequestModel _model = new UsereviewRequestModel();
        _model.setCategoryID(CategoryID);
        _model.setMemberID(_sharePrefrance.getMemberid());
        _model.setVideoID(VideoID);

        _model.setFromDate(formDate);
        _model.setToDate(toDate);


        Observable<UsereviewResponseModel> profileResponse = gerritAPI.GetUserSurvay(_model);
        //doProgressStart();
        _callbackInterface.onShowProgress();
        profileResponse.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<UsereviewResponseModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(UsereviewResponseModel categoryListModel) {

                ll_chart.setVisibility(VISIBLE);

                if (categoryListModel.getCode() == 200) {
                    FileLog.e("TAGGG", "Number converter String  here onNext>> " + categoryListModel.getLstRecords().size());
                    if (categoryListModel.getLstRecords().size() != 0) {
                        //setData(categoryListModel);

                        SimpleDateFormat fromformat = new SimpleDateFormat("MMM-dd-yyyy");
                        SimpleDateFormat toformat = new SimpleDateFormat("dd-MMM");
                        for (int i = 0; i < categoryListModel.getLstRecords().size(); i++) {
                            try {
                                Date date = fromformat.parse(categoryListModel.getLstRecords().get(i).getDate());
                                categoryListModel.getLstRecords().get(i).setDate(toformat.format(date));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }


                        LoadBarChart(categoryListModel);

                        //xAxisFormatter.DayAxisValueUpdate(categoryListModel);

                    } else {

                        SimpleDateFormat fromformat = new SimpleDateFormat("MMM-dd-yyyy");
                        SimpleDateFormat toformat = new SimpleDateFormat("dd-MMM");
                        for (int i = 0; i < categoryListModel.getLstRecords().size(); i++) {
                            try {
                                Date date = fromformat.parse(categoryListModel.getLstRecords().get(i).getDate());
                                categoryListModel.getLstRecords().get(i).setDate(toformat.format(date));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }


                        //setData(categoryListModel);
                        //generateLineData(categoryListModel);
                        LoadBarChart(categoryListModel);
                        //xAxisFormatter.DayAxisValueUpdate(categoryListModel);


                        Toast.makeText(_context, getResources().getString(R.string.txt_no_history), Toast.LENGTH_LONG).show();
                    }

                } else if (categoryListModel.getCode() == 401) {

                    ShowAuthError(categoryListModel.getMessage(), _context.getString(R.string.error_auth));

                } else {
                    Toast.makeText(_context, categoryListModel.getMessage(), Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onError(Throwable e) {
                // DismissProgress();
                _callbackInterface.onHideProgress();
            }

            @Override
            public void onComplete() {
                //  DismissProgress();
                _callbackInterface.onHideProgress();
                FileLog.e("TAGGG", "Number converter String  here onComplete>> ");
            }
        });

    }

    private void LoadBarChart(final UsereviewResponseModel categoryListModel) {

        AAChartModel aaChartModel = configureTheAAChartModel(categoryListModel);
        chartdata.aa_drawChartWithChartModel(aaChartModel);

        /*chartdata.getDescription().setEnabled(false);
        chartdata.setBackgroundColor(Color.WHITE);
        chartdata.setDrawGridBackground(false);
        chartdata.setDrawBarShadow(false);
        chartdata.setHighlightFullBarEnabled(false);

        // draw bars behind lines
        chartdata.setDrawOrder(new CombinedChart.DrawOrder[]{
                CombinedChart.DrawOrder.LINE
        });

        Legend l = chartdata.getLegend();
        l.setWordWrapEnabled(true);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);

        YAxis rightAxis = chartdata.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        rightAxis.setEnabled(false);

        YAxis leftAxis = chartdata.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        XAxis xAxis = chartdata.getXAxis();
        xAxis.setLabelRotationAngle(-25);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0f);
        *//*if (categoryListModel.getLstRecords().size() < 15) {
            xAxis.setGranularity(1f);
        } else {
            xAxis.setGranularity(1f);
        }*//*

        xAxis.setGranularity(1f);

        //xAxis.setLabelsToSkip(5);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                Log.e("TAGGG", "Check Value here > " + value);
                String Xvalue = "";
                for (int index = 0; index < categoryListModel.getLstRecords().size(); index++) {
                    if ((int) value == index) {
                        Xvalue = categoryListModel.getLstRecords().get(index).getDate();
                        break;
                    }
                }
                return Xvalue;
            }
        });

        CombinedData data = new CombinedData();

        data.setData(generateLineData(categoryListModel));

        //data.setValueTypeface(tfLight);

        xAxis.setAxisMaximum(data.getXMax());

        chartdata.getLegend().setEnabled(true);
        chartdata.setData(data);
        chartdata.invalidate();*/
    }

    public AAChartModel configureTheAAChartModel(UsereviewResponseModel categoryListModel) {

        String[] xValue = new String[categoryListModel.getLstRecords().size()];
        Object[] yValue = new Object[categoryListModel.getLstRecords().size()];
        for (int i = 0; i < categoryListModel.getLstRecords().size(); i++) {
            xValue[i] = categoryListModel.getLstRecords().get(i).getDate();
            yValue[i] = categoryListModel.getLstRecords().get(i).getPopupValue();
        }

        Object[] stopsArr = new Object[]{new Object[]{0, "rgba(00,00,00,0.5)"}, new Object[]{1, "rgba(00,00,00,0.5)"}};
        HashMap linearGradientColor = AAGradientColor.INSTANCE.linearGradient(AALinearGradientDirection.ToBottom, stopsArr);
        return (new AAChartModel()).chartType(AAChartType.Areaspline).title("").subtitle("").categories(xValue).yAxisTitle("").axesTextColor("rgba(00,00,00,0.5)").markerRadius(8.0F).markerSymbolStyle(AAChartSymbolStyleType.Normal).markerSymbol(AAChartSymbolType.Circle).yAxisLineWidth(1F).yAxisGridLineWidth(1F).legendEnabled(true).series(new AASeriesElement[]{(new AASeriesElement()).name(_survay_Detail.getPopUp_Text()).lineWidth(2F).color("rgba(00, 00, 00, 1)").fillColor(linearGradientColor).data(yValue)});
    }


    private LineData generateLineData(UsereviewResponseModel categoryListModel) {

        LineData d = new LineData();

        ArrayList<Entry> entries = new ArrayList<>();

        for (int index = 0; index < categoryListModel.getLstRecords().size(); index++)
            entries.add(new Entry(index, categoryListModel.getLstRecords().get(index).getPopupValue()));

        LineDataSet set = new LineDataSet(entries, VideoName);
        set.setColor(getResources().getColor(R.color.black));
        set.setLineWidth(3f);
        set.setCircleColor(getResources().getColor(R.color.black));
        set.setCircleRadius(5f);
        set.setFillColor(getResources().getColor(R.color.black));
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setDrawValues(false);
        set.setValueTextSize(0f);
        set.setValueTextColor(getResources().getColor(R.color.black));


        set.setDrawFilled(true);
        if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(_context, R.drawable.fill_chart);
            set.setFillDrawable(drawable);
        } else {
            set.setFillColor(R.color.black);
        }


        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setDrawCircles(true);
        d.addDataSet(set);


        return d;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_button) {
            if (img_button.getTag().equals("false")) {
                isClick = true;
                img_button.setTag("true");
                img_button.setImageResource(R.drawable.pause_red);
            } else {
                img_button.setTag("false");
                img_button.setImageResource(R.drawable.play_red);
                _callback.ForceHide(isMember);
            }
        }
    }

    public boolean ShowAuthError(String msg_title, String msg_detail) {


        TextView title = new TextView(_context);
        title.setText(msg_title);
        title.setPadding(15, 15, 15, 10);
        title.setGravity(Gravity.LEFT);
// title.setTextColor(getResources().getColor(R.color.greenBG));
        title.setTextSize(18);

        TextView msg = new TextView(_context);
        msg.setText(msg_detail);
        msg.setPadding(15, 10, 15, 10);
        msg.setGravity(Gravity.LEFT);
        msg.setTextSize(16);

        DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) { //Delete for me

                    SharedPreferencesUtility _sharePreference = new SharedPreferencesUtility(_context);
                    _sharePreference.ClearPrefrance();


                    Intent intent = new Intent(_context, SplashActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    _context.startActivity(intent);


                    dialog.dismiss();
                }

            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setCustomTitle(title);
        builder.setView(msg);
        //builder.setCancelable(true);
        builder.setPositiveButton(_context.getString(R.string.txt_logout), onClick);
        //builder.setNegativeButton(getString(R.string.txt_delete_cancel), onClick);


        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        return true;
    }
}
