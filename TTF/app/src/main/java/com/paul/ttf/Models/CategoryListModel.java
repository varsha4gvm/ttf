package com.paul.ttf.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CategoryListModel {


    @SerializedName("code")
    public Integer code;

    @SerializedName("message")
    public String message;

    @SerializedName("CategoryList")
    public ArrayList<CategoryList> CategoryList;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CategoryListModel.CategoryList> getCategoryList() {
        return CategoryList;
    }

    public void setCategoryList(ArrayList<CategoryListModel.CategoryList> categoryList) {
        CategoryList = categoryList;
    }

    public static class CategoryList implements Parcelable {

        @SerializedName("CategoryID")
        public Integer CategoryID;

        @SerializedName("CategoryName")
        public String CategoryName;

        @SerializedName("ImageName")
        public String ImageName;

        @SerializedName("ImagePath")
        public String ImagePath;

        @SerializedName("Tips")
        public String Tips;

        public Integer getCategoryID() {
            return CategoryID;
        }

        public void setCategoryID(Integer categoryID) {
            CategoryID = categoryID;
        }

        public String getCategoryName() {
            return CategoryName;
        }

        public void setCategoryName(String categoryName) {
            CategoryName = categoryName;
        }

        public String getImageName() {
            return ImageName;
        }

        public void setImageName(String imageName) {
            ImageName = imageName;
        }

        public String getImagePath() {
            return ImagePath;
        }

        public void setImagePath(String imagePath) {
            ImagePath = imagePath;
        }

        public String getTips() {
            return Tips;
        }

        public void setTips(String tips) {
            Tips = tips;
        }

        protected CategoryList(Parcel in) {
            ImagePath = in.readString();
            String[] c = new String[2];
            in.readStringArray(c);
            //in.readInt();
            CategoryName = c[0];
            Tips = c[1];
        }

        public static final Creator<CategoryList> CREATOR = new Creator<CategoryList>() {
            @Override
            public CategoryList createFromParcel(Parcel in) {
                return new CategoryList(in);
            }

            @Override
            public CategoryList[] newArray(int size) {
                return new CategoryList[size];
            }
        };


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(ImagePath);
            String[] c = new String[]{CategoryName, Tips};
            parcel.writeStringArray(c);
            //parcel.writeInt(CategoryID);
        }

    }


}
