package com.paul.ttf.Models;

import com.google.gson.annotations.SerializedName;

public class LoginRequestModel {

    @SerializedName("UserName")
    public String UserName;

    @SerializedName("Password")
    public String Password;



    @SerializedName("DeviceId")
    public String DeviceId;

    @SerializedName("DeviceName")
    public String DeviceName;

    @SerializedName("VersionNo")
    public String VersionNo;

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String deviceId) {
        DeviceId = deviceId;
    }

    public String getDeviceName() {
        return DeviceName;
    }

    public void setDeviceName(String deviceName) {
        DeviceName = deviceName;
    }

    public String getVersionNo() {
        return VersionNo;
    }

    public void setVersionNo(String versionNo) {
        VersionNo = versionNo;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}