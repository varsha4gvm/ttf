package com.paul.ttf.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.paul.ttf.InterfaceModule.CategoryVideoInterface;
import com.paul.ttf.InterfaceModule.MenuRedirect;
import com.paul.ttf.Models.MenuModel;
import com.paul.ttf.R;
import com.paul.ttf.Utility.CommonFunction;
import com.paul.ttf.Utility.SharedPreferencesUtility;
import com.google.android.material.snackbar.Snackbar;

public class HistoryFragment extends Fragment implements MenuRedirect {

    private String title;
    RecyclerView recycleList;
    RelativeLayout coordinateParent;

    SharedPreferencesUtility _shared_prefrance;
    static Integer Categoryid;
    static CategoryVideoInterface _interface;

    public static HistoryFragment createFragment(String title, Integer Categoryid, CategoryVideoInterface _interface) {
        HistoryFragment customFragment = new HistoryFragment();
        HistoryFragment.Categoryid = Categoryid;
        HistoryFragment._interface = _interface;
        customFragment.title = title;
        return customFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View _view = inflater.inflate(R.layout.tab_fragment, container, false);

        // init(_view);

        return _view;
    }

    private void init(View _view) {

        recycleList = _view.findViewById(R.id.recycle_menu);
        coordinateParent = _view.findViewById(R.id.coordinateParent);

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        int colmn = 2;
        if (tabletSize) {
            colmn = 3;
        } else {
            colmn = 2;
        }

        RecyclerView.LayoutManager mPreLayoutManager = new GridLayoutManager(getActivity(), colmn);

        recycleList.setLayoutManager(mPreLayoutManager);
        //recycleList.setHasFixedSize(true);

        _shared_prefrance = new SharedPreferencesUtility(getActivity());
        if (CommonFunction.isInternetAvailable(getActivity())) {
            //GetCategoryVideodata();
        } else {
            Snackbar snackbar = Snackbar
                    .make(coordinateParent, getString(R.string.error_internet), Snackbar.LENGTH_LONG);

            snackbar.setActionTextColor(Color.RED);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void MenuRedirectEvent(MenuModel object) {

    }

    private void fillList() {

        //  CategoryVIdeoAdapter _adapter = new CategoryVIdeoAdapter(_category_video_list, getActivity(), _interface);
        //  recycleList.setAdapter(_adapter);

    }
}
