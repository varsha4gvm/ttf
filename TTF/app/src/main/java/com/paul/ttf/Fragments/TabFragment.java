package com.paul.ttf.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.paul.ttf.APIThread.APIClient;
import com.paul.ttf.APIThread.APIInterface;
import com.paul.ttf.Adapter.CategoryVIdeoAdapter;
import com.paul.ttf.InterfaceModule.CategoryVideoInterface;
import com.paul.ttf.InterfaceModule.MenuRedirect;
import com.paul.ttf.Models.CategoryVideoModel;
import com.paul.ttf.Models.MenuModel;
import com.paul.ttf.R;
import com.paul.ttf.Utility.CommonFunction;
import com.paul.ttf.Utility.Constants;
import com.paul.ttf.Utility.FileLog;
import com.paul.ttf.Utility.SharedPreferencesUtility;
import com.paul.ttf.databinding.TabFragmentBinding;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TabFragment extends Fragment implements MenuRedirect {

    private String title;


    SharedPreferencesUtility _shared_prefrance;
    static Integer Categoryid;
    static CategoryVideoInterface _interface;
    TabFragmentBinding binding;

    public ArrayList<CategoryVideoModel.Video_List> _category_video_list;

    public static TabFragment createFragment(String title, Integer Categoryid, CategoryVideoInterface _interface) {
        TabFragment customFragment = new TabFragment();
        TabFragment.Categoryid = Categoryid;
        TabFragment._interface = _interface;
        customFragment.title = title;


        return customFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // View _view = inflater.inflate(R.layout.tab_fragment, container, false);

        binding = DataBindingUtil.inflate(
                inflater, R.layout.tab_fragment, container, false);
        View view = binding.getRoot();
        _category_video_list = new ArrayList<>();
        //here data must be an instance of the class MarsDataProvider

        init(view);

        return view;
    }

    private void init(View _view) {

        // recycleList = _view.findViewById(R.id.recycle_menu);
        // coordinateParent = _view.findViewById(R.id.coordinateParent);


        _shared_prefrance = new SharedPreferencesUtility(getActivity());
        if (CommonFunction.isInternetAvailable(getActivity())) {
            GetCategoryVideodata();
        } else {
            Snackbar snackbar = Snackbar
                    .make(binding.coordinateParent, getString(R.string.error_internet), Snackbar.LENGTH_LONG);

            snackbar.setActionTextColor(Color.RED);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    private void GetCategoryVideodata() {

        int Sampleid = 1;
        APIInterface gerritAPI;
        if (_shared_prefrance.getMemberid() != 0) {
            Sampleid = 0;
            gerritAPI = APIClient.getClientHeader(Constants.Guest_Token).create(APIInterface.class);
        } else {
            gerritAPI = APIClient.getClientHeader(Constants.Guest_Token).create(APIInterface.class);
        }
        String Category_uri = Constants.BASE_URL + Constants.GET_CATEGORY_VIDEO_URL + "?CategoryID=" + Categoryid + "&IsSample=" + Sampleid;
        Observable<CategoryVideoModel> profileResponse = gerritAPI.getCategoryVideoList(Category_uri);

        _interface.onShowProgress();
        profileResponse.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<CategoryVideoModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(CategoryVideoModel categoryListModel) {
                FileLog.e("TAGGG", "Number converter String here onNext>> " + categoryListModel.getVideo_List().size());

                if (categoryListModel.getCode() == 200) {
                    _category_video_list = categoryListModel.getVideo_List();
                    fillList();
                    if (_category_video_list.size() == 0) {
                        Snackbar snackbar = Snackbar
                                .make(binding.coordinateParent, "Video not available.!", Snackbar.LENGTH_LONG);

                        snackbar.setActionTextColor(Color.RED);
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    }
                } else {
                    Snackbar snackbar = Snackbar
                            .make(binding.coordinateParent, categoryListModel.getMessage(), Snackbar.LENGTH_LONG);

                    snackbar.setActionTextColor(Color.RED);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                }

            }

            @Override
            public void onError(Throwable e) {
                _interface.onHideProgress();
            }

            @Override
            public void onComplete() {
                _interface.onHideProgress();
                FileLog.e("TAGGG", "Number converter String  here onComplete>> ");
            }
        });

    }


    @Override
    public void MenuRedirectEvent(MenuModel object) {

    }

    private void fillList() {
        FileLog.e("TAGGGG", "fillList size > " + _category_video_list.size());
        CategoryVIdeoAdapter _adapter = new CategoryVIdeoAdapter(_category_video_list, getActivity(), _interface, true, Categoryid);
        binding.recycleMenu.setAdapter(_adapter);

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        int colmn = 2;
        if (tabletSize) {
            colmn = 3;
        } else {
            colmn = 2;
        }

        RecyclerView.LayoutManager mPreLayoutManager = new GridLayoutManager(getActivity(), colmn);

        binding.recycleMenu.setLayoutManager(mPreLayoutManager);
        binding.recycleMenu.setHasFixedSize(true);

    }
}
