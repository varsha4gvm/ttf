package com.paul.ttf.Utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.paul.ttf.BuildConfig;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Administrator on 11/13/2018.
 */
public class CommonFunction {

    public static void showAlertDialog(Activity activity, String title, String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
        builder1.setTitle(title);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }


    public static void addPrefrenceString(Context context, String value,
                                          String key) {
        SharedPreferences prefrence = context.getSharedPreferences("Prefrence",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefrence.edit();
        FileLog.e("TAGGG", "updateNavigationDrawer SharedPreferences > " + value + "< K " + key);
        edit.putString(key, value);
        edit.commit();
    }

    public static void addPrefrenceBoolean(Context context, boolean value,
                                           String key) {
        SharedPreferences prefrence = context.getSharedPreferences("PrefrenceBoolean",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefrence.edit();

        edit.putBoolean(key, value);
        edit.commit();
    }

    public static double CalculateDiscountPrice(double discount, double TotalAmount) {


        double discountprice = 0;


        discountprice = TotalAmount * discount / 100;


        return discountprice;

    }

    public static String getDeviceUUID() {
        String uniqueID = UUID.randomUUID().toString();
        return uniqueID;
    }

    public static String getDeviceName() {
        String str = android.os.Build.MODEL;
        return str;
    }

    public static String getAppVersion() {
        String version = BuildConfig.VERSION_NAME;
        return version;
    }

    public static String getCurrentDate() {
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        return df.format(date);
    }

}
