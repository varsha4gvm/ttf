package com.paul.ttf.Utility;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesUtility {

    SharedPreferences.Editor editor;
    SharedPreferences pref;

    String Token = "token", Memberid = "memberid", Emailid = "emailid", Firstname = "firstname", Surname = "surname", Gender = "gender", Joindate = "joindate", Memberphoto = "memberphoto",Password = "password",CurrentDate = "currentdate";


    public SharedPreferencesUtility(Context context) {
        pref = context.getSharedPreferences("groupchat", Context.MODE_PRIVATE); // 0 - for private mode
        editor = pref.edit();
    }


    public int getMemberid() {
        return pref.getInt(Memberid, 0);
    }

    public void setMemberid(int memberid) {
        editor.putInt(Memberid, memberid); // Storing integer
        editor.commit();
    }

    public String getEmailid() {
        return pref.getString(Emailid, "");
    }

    public void setEmailid(String token) {
        editor.putString(Emailid, token); // Storing integer
        editor.commit();
    }


    public String getFirstname() {
        return pref.getString(Firstname, "");
    }

    public void setFirstname(String firstname) {
        editor.putString(Firstname, firstname);
        editor.commit();
    }

    public String getSurname() {
        return pref.getString(Surname, "");
    }

    public void setSurname(String surname) {
        editor.putString(Surname, surname);
        editor.commit();
    }

    public String getGender() {
        return pref.getString(Gender, "");
    }

    public void setGender(String gender) {
        editor.putString(Gender, gender);
        editor.commit();
    }

    public String getJoindate() {
        return pref.getString(Joindate, "");
    }

    public void setJoindate(String joindate) {
        editor.putString(Joindate, joindate);
        editor.commit();
    }

    public String getMemberphoto() {
        return pref.getString(Memberphoto, "");
    }

    public void setMemberphoto(String memberphoto) {
        editor.putString(Memberphoto, memberphoto);
        editor.commit();
    }

    public String getToken() {
        return pref.getString(Token, "");
    }

    public void setToken(String token) {
        editor.putString(Token, token);
        editor.commit();
    }

    public String getPassword() {
        return pref.getString(Password, "");
    }

    public void setPassword(String password) {
        editor.putString(Password, password);
        editor.commit();
    }

    public String getCurrentDate() {
        return pref.getString(CurrentDate, "");
    }

    public void setCurrentDate(String currentDate) {
        editor.putString(CurrentDate, currentDate);
        editor.commit();
    }

    public void ClearPrefrance() {

        editor.putString(this.Emailid, ""); // Set String
        editor.putInt(Memberid, 0); // Storing integer
        editor.putString(Token, ""); // Storing String
        editor.putString(Password, ""); // Storing String

        editor.putString(Firstname, ""); // Storing String
        editor.putString(Surname, ""); // Storing String
        editor.putString(Gender, ""); // Storing String
        editor.putString(Joindate, ""); // Storing String
        editor.putString(Memberphoto, ""); // Storing String


        editor.commit();
    }
}
