package com.paul.ttf.CustomControl;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import com.paul.ttf.R;
import com.paul.ttf.UI.ApplicationClass;


public class CustomTextView extends TextView {

    private int typefaceType;


    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);
        try {
            typefaceType = array.getInteger(R.styleable.CustomTextView_font_name, 0);
            //typefaceType = array.getInteger(0, 0);
        } finally {
            array.recycle();
        }
        if (!isInEditMode()) {
            setTypeface(ApplicationClass.getApp().getTypeFace(typefaceType));
        }
    }
}