package com.paul.ttf.InterfaceModule;

import com.paul.ttf.exoplayer.SimpleExoPlayer;

public interface MuteUnmuteInterface {

    public void MuteAudio();

    public void UnMuteAudio();

}
