/**
 * Copyright 2014 Google
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.paul.ttf;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.paul.ttf.APIThread.APIClient;
import com.paul.ttf.APIThread.APIInterface;
import com.paul.ttf.Adapter.CategoryVIdeoAdapter;
import com.paul.ttf.Adapter.CustomVideoAdapter;
import com.paul.ttf.CustomControl.DatePicker.CalendarPickerView;

import com.paul.ttf.Extract.ExtractorException;
import com.paul.ttf.Extract.YoutubeStreamExtractor;
import com.paul.ttf.Extract.model.YTMedia;
import com.paul.ttf.Extract.model.YTSubtitles;
import com.paul.ttf.Extract.utils.LogUtils;
import com.paul.ttf.InterfaceModule.CategoryVideoInterface;

import com.paul.ttf.Models.AllCategoryVideoModel;
import com.paul.ttf.Models.CategoryListModel;
import com.paul.ttf.Models.CategoryVideoModel;
import com.paul.ttf.Models.UsereviewRequestModel;
import com.paul.ttf.Models.UsereviewResponseModel;
import com.paul.ttf.UI.ApplicationClass;
import com.paul.ttf.UI.SplashActivity;
import com.paul.ttf.Utility.CommonFunction;
import com.paul.ttf.Utility.Constants;
import com.paul.ttf.Utility.FileLog;
import com.paul.ttf.Utility.SharedPreferencesUtility;
import com.paul.ttf.databinding.ActivityCategoryDetailBinding;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.paul.ttf.exoplayer.SimpleExoPlayer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class CategoryDetailActivity extends AppCompatActivity implements CategoryVideoInterface, View.OnClickListener, View.OnTouchListener {

    public static final String Category = "movie";
    private static final String LOG_TAG = "DetailsActivity";
    CategoryVIdeoAdapter _adapter;
    ArrayList<CategoryVideoModel.Video_List> _category_video_list;
    CategoryListModel.CategoryList category;
    Integer Categoryid;

    AllCategoryVideoModel.List.VideoList _video_obj = null;
    ActivityCategoryDetailBinding binding;
    private CompositeDisposable disposables = new CompositeDisposable();
    public static Activity _sample;
    ExoPlayerManager _manager;
    boolean callFromCreate = false;
    SharedPreferencesUtility _shared_prefrance;
    private Dialog mFullScreenDialog;
    private boolean mExoPlayerFullscreen = false;
    Dialog mDialog;
    String From_date = "", To_date = "";
    //  DayAxisValueFormatter xAxisFormatter;

    private ArrayList<String> urls_li;
    boolean isMemberUser = true;
    boolean isRedirect = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportPostponeEnterTransition();
        final long start = System.currentTimeMillis();
        _shared_prefrance = new SharedPreferencesUtility(CategoryDetailActivity.this);
        setContentView(R.layout.activity_category_detail);
        _sample = this;
        callFromCreate = true;
        binding =
                DataBindingUtil.setContentView(this, R.layout.activity_category_detail);
        setupBackdropHeight(binding);

        setSupportActionBar(binding.toolbar);
        setUpNavigationView();
        Categoryid = getIntent().getIntExtra("categoryid", 0);


        category = getIntent().getParcelableExtra(Category);

        Glide.with(this)
                .load(category.getImagePath())
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .fitCenter()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        CategoryDetailActivity.this.supportStartPostponedEnterTransition();
                        Log.d(LOG_TAG, "onResourceReady: " + (System.currentTimeMillis() - start));
                        return false;
                    }
                })
                .into(binding.backdrop);
        urls_li = new ArrayList<>();
        _category_video_list = new ArrayList<>();

        init(binding, category);
    }

    private void setUpNavigationView() {


        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        binding.toolbar.setTitle("");
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // perform whatever you want on back arrow click
                onBackPressed();
            }
        });
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu


        binding.toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);
    }

    private void init(ActivityCategoryDetailBinding binding, CategoryListModel.CategoryList movie) {
        //binding.txtTipsTitle.setText(movie.getDescription());

        binding.txtTipsTitle.setText(movie.getTips());

        TextView tvToolbarTitle = (TextView) binding.toolbar.findViewById(R.id.text_remain_time);
        tvToolbarTitle.setText(movie.getCategoryName());
        tvToolbarTitle.setTextColor(getResources().getColor(R.color.white));
        //binding.toolbar.setTitle(movie.getCategoryName());
        //binding.toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        isMemberUser = true;
        if (_shared_prefrance.getMemberid() == 0) {
            isMemberUser = false;
            binding.tabLinearHistory.setVisibility(View.GONE);
            binding.tabLinearVideo.setVisibility(View.GONE);
        }

        binding.mPlayerView.AssignObject(this, isMemberUser);


        if (CommonFunction.isInternetAvailable(CategoryDetailActivity.this)) {
            GetCategoryVideodata();
        } else {
            Snackbar snackbar = Snackbar
                    .make(binding.coordinateParent, getString(R.string.error_internet), Snackbar.LENGTH_LONG);

            snackbar.setActionTextColor(Color.RED);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        }

        // fillList();

        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        int colmn = 2;
        if (tabletSize) {
            colmn = 3;
        } else {
            colmn = 2;
        }

        RecyclerView.LayoutManager mPreLayoutManager = new GridLayoutManager(CategoryDetailActivity.this, colmn);

        binding.recyclerviewVideos.setLayoutManager(mPreLayoutManager);
        binding.recyclerviewVideos.setHasFixedSize(true);


        binding.tabLinearVideo.setOnClickListener(this);
        binding.tabLinearHistory.setOnClickListener(this);
        binding.editFromdate.setOnTouchListener(this);
        binding.btnLogin.setOnClickListener(this);


        initFullscreenDialog();
        ConfigureProgress();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                InitConfigure();
                //LoadBarChart();
            }
        }, 500);

    }

    private void InitConfigure() {

        final String TAG = "dialogSorryCall_dialog";
        TextView tvMessage, tvYes, tvNo;
        LayoutInflater inflater = LayoutInflater.from(CategoryDetailActivity.this);
        mDialog = new Dialog(CategoryDetailActivity.this,
                android.R.style.Widget_Material);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);

        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.dialog_logout, null);

        final Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 10);

        final Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -10);


        final CalendarPickerView calendar = dialoglayout.findViewById(R.id.calendar_view);
        Button get_selected_dates = dialoglayout.findViewById(R.id.get_selected_dates);
        ArrayList<Integer> list = new ArrayList<>();
        // list.add(2);


        calendar.deactivateDates(list);
        ArrayList<Date> arrayList = new ArrayList<>();
        try {
            SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");

            String strdate = "22-4-2019";
            String strdate2 = "26-4-2019";

            Date newdate = dateformat.parse(strdate);
            Date newdate2 = dateformat.parse(strdate2);
            arrayList.add(newdate);
            arrayList.add(newdate2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        calendar.init(lastYear.getTime(), nextYear.getTime(), new SimpleDateFormat("MMMM, yyyy", Locale.getDefault())) //
                .inMode(CalendarPickerView.SelectionMode.RANGE); //
                /*.withDeactivateDates(list)
                .withSubTitles(getSubTitles())
                .withHighlightedDates(arrayList);*/

        calendar.scrollToDate(new Date());
        mDialog.setContentView(dialoglayout);
        mDialog.setCanceledOnTouchOutside(false);

        get_selected_dates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                for (int i = 0; i < calendar.getSelectedDates().size(); i++) {
                    FileLog.e("TAGGG", "okhttp check result > " + calendar.getSelectedDates().get(i));
                    if (i == 0) {
                        From_date = DateFormat.format("yyyy-MM-dd", new Date(calendar.getSelectedDates().get(i).getTime())).toString();
                        FileLog.e("TAGGG", "okhttp check result First> " + calendar.getSelectedDates().get(i).getTime() + " Convert " + From_date);
                        binding.editFromdate.setText(From_date);
                    }
                    if (i == (calendar.getSelectedDates().size() - 1)) {
                        To_date = DateFormat.format("yyyy-MM-dd", new Date(calendar.getSelectedDates().get(i).getTime())).toString();
                        FileLog.e("TAGGG", "okhttp check result Last> " + calendar.getSelectedDates().get(i).getTime() + " Convert " + To_date);
                        binding.editFromdate.setText(Html.fromHtml("<b>From </b> " + binding.editFromdate.getText().toString() + " <b>To</b>  " + To_date));
                    }
                }
            }
        });
    }

    private void LoadBarChart(final UsereviewResponseModel categoryListModel) {

        binding.chart.getDescription().setEnabled(false);
        binding.chart.setBackgroundColor(Color.WHITE);
        binding.chart.setDrawGridBackground(false);
        binding.chart.setDrawBarShadow(false);
        binding.chart.setHighlightFullBarEnabled(false);

        // draw bars behind lines
        binding.chart.setDrawOrder(new CombinedChart.DrawOrder[]{
                CombinedChart.DrawOrder.LINE
        });

        Legend l = binding.chart.getLegend();
        l.setWordWrapEnabled(true);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);

        /*YAxis rightAxis = binding.chart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)*/

        YAxis leftAxis = binding.chart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        XAxis xAxis = binding.chart.getXAxis();
        //xAxis.setLabelRotationAngle(-45);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0f);
        Log.e("TAGGG", "Check Value here > " + (categoryListModel.getLstRecords().size() < 15));
        if (categoryListModel.getLstRecords().size() < 15) {
            xAxis.setGranularity(3f);
        } else {
            xAxis.setGranularity(5f);
        }

        //xAxis.setLabelsToSkip(5);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                Log.e("TAGGG", "Check Value here > " + value);
                String Xvalue = "";
                for (int index = 0; index < categoryListModel.getLstRecords().size(); index++) {
                    if ((int) value == index) {
                        Xvalue = categoryListModel.getLstRecords().get(index).getDate();
                        break;
                    }
                }
                return Xvalue;
            }
        });

        CombinedData data = new CombinedData();

        data.setData(generateLineData(categoryListModel));

        //data.setValueTypeface(tfLight);

        xAxis.setAxisMaximum(data.getXMax() + 0.25f);

        binding.chart.getLegend().setEnabled(false);
        binding.chart.setData(data);

        binding.chart.invalidate();
    }

    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }

    private void setupBackdropHeight(ActivityCategoryDetailBinding binding) {
        CollapsingToolbarLayout.LayoutParams params = new CollapsingToolbarLayout.LayoutParams(MATCH_PARENT,
                getBackdropHeight());
        params.setCollapseMode(CollapsingToolbarLayout.LayoutParams.COLLAPSE_MODE_PARALLAX);

        binding.backdrop.setLayoutParams(params);

    }

    public int getBackdropHeight() {
        return (int) (getResources().getDisplayMetrics().widthPixels / 1.5f);
    }


    @Override
    public void onSelectCategoryItem(CategoryVideoModel.Video_List position_obj) {
        ResetList();
        binding.mPlayerView.HideDialog();
        if (_manager != null && _manager.isPlayerPlaying()) {
            int Videoid = (int) binding.mPlayerView.getTag();
            if (Videoid == position_obj.getVideoID()) {
                position_obj.setPause(false);
                _manager.stopPlayer(true);
            } else {
                releasePlayer();

                for (int i = 0; i < position_obj.settimepopup.size(); i++) {
                    position_obj.settimepopup.get(i).setAdsSeconds(getSecondsFormat(position_obj.settimepopup.get(i).getTime()));
                }
                position_obj.setPause(true);
                ExtractVideo(position_obj);
            }
        } else if (_manager != null && (!_manager.isPlayerPlaying())) {
            int Videoid = (int) binding.mPlayerView.getTag();
            if (Videoid == position_obj.getVideoID()) {
                _manager.stopPlayer(false);
                position_obj.setPause(true);
            } else {
                releasePlayer();

                for (int i = 0; i < position_obj.settimepopup.size(); i++) {
                    position_obj.settimepopup.get(i).setAdsSeconds(getSecondsFormat(position_obj.settimepopup.get(i).getTime()));
                }
                position_obj.setPause(true);
                ExtractVideo(position_obj);
            }
        } else {

            for (int i = 0; i < position_obj.settimepopup.size(); i++) {
                position_obj.settimepopup.get(i).setAdsSeconds(getSecondsFormat(position_obj.settimepopup.get(i).getTime()));
            }
            position_obj.setPause(true);
            ExtractVideo(position_obj);
        }

        UpdateList(position_obj);
    }

    private void ResetList() {
        for (int i = 0; i < _category_video_list.size(); i++) {
            _category_video_list.get(i).setPause(false);
        }
        _adapter.notifyDataSetChanged();
    }

    private void UpdateList(CategoryVideoModel.Video_List position_obj) {
        for (int i = 0; i < _category_video_list.size(); i++) {
            if (position_obj.getVideoID() == _category_video_list.get(i).getVideoID()) {
                _category_video_list.get(i).setPause(position_obj.isPause());
                break;
            }
        }
        _adapter.notifyDataSetChanged();
    }

    @Override
    public void onShowToastMessage(String Message) {
        Snackbar snackbar = Snackbar
                .make(binding.coordinateParent, Message, Snackbar.LENGTH_LONG);

        snackbar.setActionTextColor(Color.RED);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    @Override
    public void onShowProgress() {
        doProgressStart();
    }

    @Override
    public void onHideProgress() {
        DismissProgress();
    }


    private void GetCategoryVideodata() {

        int Sampleid = 1;
        APIInterface gerritAPI;
        if (!_shared_prefrance.getToken().equals("")) {
            Sampleid = 0;
            gerritAPI = APIClient.getClientHeader(_shared_prefrance.getToken()).create(APIInterface.class);
        } else {
            gerritAPI = APIClient.getClientHeader(Constants.Guest_Token).create(APIInterface.class);
        }
        String Category_uri = Constants.BASE_URL + Constants.GET_CATEGORY_VIDEO_URL + "?CategoryID=" + Categoryid + "&IsSample=" + Sampleid;
        Observable<CategoryVideoModel> profileResponse = gerritAPI.getCategoryVideoList(Category_uri);

        doProgressStart();
        profileResponse.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<CategoryVideoModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(CategoryVideoModel categoryListModel) {


                if (categoryListModel.getCode() == 200) {
                    FileLog.e("TAGGG", "Number converter String  here onNext>> " + categoryListModel.getVideo_List().size());
                    _category_video_list = categoryListModel.getVideo_List();


                    //ArrayList<AllCategoryVideoModel.List.VideoList> videoListModel = new ArrayList<>();
                   /* for (int i = 0; i < _category_video_list.size(); i++) {
                        AllCategoryVideoModel.List.VideoList _object = new AllCategoryVideoModel.List.VideoList();
                        _object.setVideoID(_category_video_list.get(i).getVideoID());
                        _object.setVideoName(_category_video_list.get(i).getVideoName());
                        videoListModel.add(_object);
                    }*/

                    boolean isTimeStampShow = false;
                    try {


                        for (int i = 0; i < _category_video_list.size(); i++) {
                            for (int j = 0; j < _category_video_list.get(i).getSettimepopup().size(); j++) {
                                isTimeStampShow = true;
                                break;
                            }
                        }
                        if (!isTimeStampShow) {
                            binding.tabLinearHistory.setVisibility(View.GONE);
                        }
                    } catch (Exception ex) {

                    }
                    fillList(isTimeStampShow);
                    FillVideoSpinner(_category_video_list);
                    if (_category_video_list.size() == 0) {
                        Snackbar snackbar = Snackbar
                                .make(binding.coordinateParent, "Video not available.!", Snackbar.LENGTH_LONG);

                        snackbar.setActionTextColor(Color.RED);
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    }
                } else if (categoryListModel.getCode() == 401) {

                    ShowAuthError(categoryListModel.getMessage(), getString(R.string.error_auth));

                } else {
                    Snackbar snackbar = Snackbar
                            .make(binding.coordinateParent, categoryListModel.getMessage(), Snackbar.LENGTH_LONG);

                    snackbar.setActionTextColor(Color.RED);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                }

            }

            @Override
            public void onError(Throwable e) {
                DismissProgress();
            }

            @Override
            public void onComplete() {
                DismissProgress();
                FileLog.e("TAGGG", "Number converter String  here onComplete>> ");
            }
        });

    }

    private void fillList(boolean isTimeStampShow) {

        _adapter = new CategoryVIdeoAdapter(_category_video_list, CategoryDetailActivity.this, this, isMemberUser, Categoryid);
        binding.recyclerviewVideos.setAdapter(_adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        isRedirect = false;
        FileLog.e("TAGGG", "Check vlogs > onResume() " + (_manager != null));
        if (!callFromCreate) {
            binding.loadingViewRepeat.setVisibility(View.GONE);
        } else {
            callFromCreate = false;
        }

        try {
            if (_manager != null) {
                _manager.stopPlayer(false);
            }
        } catch (Exception ex) {
        }
    }

    @Override
    protected void onPause() {
        FileLog.e("TAGGG", "Check vlogs > onPause() " + (_manager != null));
        isRedirect = true;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.loadingViewRepeat.setVisibility(View.GONE);
            }
        });

        try {
            if (_manager != null) {
                if (_manager.isPlayerPlaying()) {
                    _manager.stopPlayer(true);
                }
            }
        } catch (Exception ex) {

        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {

            FileLog.e("TAGGG", "Number converter String Call 1");
            disposables.clear(); // clearing it : do not emit after destroy

           /* runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    binding.loadingViewRepeat.setVisibility(View.GONE);
                }
            });*/
            releasePlayer();

        } catch (Exception e) {

        }
    }

    private void doProgressStart() {
        FileLog.e("TAGGG", "okhttp Call Any where ");


        binding.loadingViewRepeat.setVisibility(View.VISIBLE);
        disposables.add(getObservable()
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getObserver()));
    }

    private DisposableObserver<Long> getObserver() {
        return new DisposableObserver<Long>() {

            @Override
            public void onNext(Long value) {
                binding.loadingViewRepeat.startAnimation();

            }

            @Override
            public void onError(Throwable e) {
                DismissProgress();
            }

            @Override
            public void onComplete() {
                DismissProgress();
            }
        };
    }

    private void ConfigureProgress() {

        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
        int marvel_1 = R.drawable.marvel_1_lollipop;
        int marvel_2 = R.drawable.marvel_2_lollipop;
        int marvel_3 = R.drawable.marvel_3_lollipop;
        int marvel_4 = R.drawable.marvel_4_lollipop;
        binding.loadingViewRepeat.addAnimation(Color.parseColor("#FFD200"), marvel_1,
                LoadingView.FROM_LEFT);
        binding.loadingViewRepeat.addAnimation(Color.parseColor("#2F5DA9"), marvel_2,
                LoadingView.FROM_TOP);
        binding.loadingViewRepeat.addAnimation(Color.parseColor("#FF4218"), marvel_3,
                LoadingView.FROM_RIGHT);
        binding.loadingViewRepeat.addAnimation(Color.parseColor("#C7E7FB"), marvel_4,
                LoadingView.FROM_BOTTOM);

        binding.loadingViewRepeat.addListener(new LoadingView.LoadingListener() {
            @Override
            public void onAnimationStart(int currentItemPosition) {
                FileLog.e("TAGGG", "Number converter String Call > onAnimationStart");
            }

            @Override
            public void onAnimationRepeat(int nextItemPosition) {
                FileLog.e("TAGGG", "Number converter String Call > onAnimationRepeat");
            }

            @Override
            public void onAnimationEnd(int nextItemPosition) {
                FileLog.e("TAGGG", "Number converter String Call > onAnimationEnd");
            }
        });
    }

    private void DismissProgress() {
        FileLog.e("TAGGG", "Number converter String Call 2");

        binding.loadingViewRepeat.setVisibility(View.GONE);
        disposables.clear(); // clearing it : do not emit after destroy
    }

    private Observable<? extends Long> getObservable() {
        return Observable.interval(0, 500, TimeUnit.MILLISECONDS);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.tab_linear_video) {
            binding.tabLinearVideo.setBackground(getResources().getDrawable(R.drawable.manu_bottom_color));
            binding.tabLinearHistory.setBackground(getResources().getDrawable(R.drawable.manu_bottom_unselect_color));
            binding.txtVideo.setTextColor(getResources().getColor(R.color.fab_color));
            binding.txtHistory.setTextColor(getResources().getColor(R.color.default_background));
            binding.linearVideoList.setVisibility(View.VISIBLE);
            binding.linearfromdate.setVisibility(View.GONE);
        } else if (view.getId() == R.id.tab_linear_history) {
            binding.tabLinearHistory.setBackground(getResources().getDrawable(R.drawable.manu_bottom_color));
            binding.tabLinearVideo.setBackground(getResources().getDrawable(R.drawable.manu_bottom_unselect_color));
            binding.linearVideoList.setVisibility(View.GONE);
            binding.linearfromdate.setVisibility(View.VISIBLE);
            binding.txtVideo.setTextColor(getResources().getColor(R.color.default_background));
            binding.txtHistory.setTextColor(getResources().getColor(R.color.fab_color));
        } else if (view.getId() == R.id.btn_login) {
            if (_video_obj != null) {
                if (IsValid()) {
                    GetUserSurvaydata();
                }
            } else {
                Snackbar snackbar = Snackbar
                        .make(binding.coordinateParent, getResources().getString(R.string.txt_no_video), Snackbar.LENGTH_LONG);

                snackbar.setActionTextColor(Color.RED);
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        }

    }



    private void ExtractVideo(final CategoryVideoModel.Video_List YOUTUBE_VIDEO) {

        FileLog.e("TAGGG", "Got a result with > onSuccess <  " + YOUTUBE_VIDEO.getYoutubeVideoID());
        String youtubeLink = "http://youtube.com/watch?v=" + YOUTUBE_VIDEO.getYoutubeVideoID();
        Toast.makeText(getApplicationContext(), "Please wait", Toast.LENGTH_LONG).show();

        new YouTubeExtractor(this) {
            @Override
            public void onExtractionComplete(SparseArray<YtFile> ytFiles, VideoMeta vMeta) {
                if (ytFiles != null) {
                    int itag = 22;

                    binding.mPlayerView.setVisibility(View.VISIBLE);
                    binding.mainMediaFrame.setVisibility(View.VISIBLE);

                    String downloadUrl = ytFiles.get(itag).getUrl();

                    String videoUrl = downloadUrl;

                    binding.mPlayerView.setTag(YOUTUBE_VIDEO.getVideoID());
                    binding.mPlayerView.getControllerShowTimeoutMs();
                    _manager = ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this);


                    binding.mPlayerView.setPlayer(_manager.getPlayerView().getPlayer());
                    binding.mPlayerView.UpdateObject(YOUTUBE_VIDEO, CategoryDetailActivity.this);
                    ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this).playStream(videoUrl);

                    if (isRedirect) {
                        if (_manager != null) {
                            _manager.stopPlayer(true);
                        }
                    }
                }
            }
        }.extract(youtubeLink, true, true);

     /*   new YoutubeStreamExtractor(new YoutubeStreamExtractor.ExtractorListner() {


            @Override
            public void onExtractionGoesWrong(final ExtractorException e) {

                FileLog.e("TAGGG", "Got a result with > Error <  " + e.getMessage() ,e);
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();


                String url = "https://www.radiantmediaplayer.com/media/bbb-360p.mp4";

                //PlayVideo(url);

                binding.mPlayerView.setVisibility(View.VISIBLE);
                binding.mainMediaFrame.setVisibility(View.VISIBLE);

                String videoUrl = url;

                binding.mPlayerView.setTag(YOUTUBE_VIDEO.getVideoID());
                binding.mPlayerView.getControllerShowTimeoutMs();
                _manager = ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this);

                binding.mPlayerView.setPlayer(_manager.getPlayerView().getPlayer());
                binding.mPlayerView.UpdateObject(YOUTUBE_VIDEO, CategoryDetailActivity.this);
                ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this).playStream(videoUrl);

                if (isRedirect) {
                    if (_manager != null) {
                        _manager.stopPlayer(true);
                    }
                }

            }

            @Override
            public void onExtractionDone(List<com.paul.ttf.Extract.model.YTMedia> adativeStream, List<com.paul.ttf.Extract.model.YTMedia> muxedStream, List<YTSubtitles> subList, com.paul.ttf.Extract.model.YoutubeMeta meta) {

                //url to get subtitle
                //String subUrl=subtitles.get(0).getBaseUrl();


                urls_li.clear();
                for (YTMedia c : muxedStream) {

                    urls_li.add(c.getUrl());
                    // adapter.notifyDataSetChanged();
                }
                for (YTMedia media : adativeStream) {

                    urls_li.add(media.getUrl());
                    // adapter.notifyDataSetChanged();
                }
                //Toast.makeText(getApplicationContext(), meta.getTitle(), Toast.LENGTH_LONG).show();
                //Toast.makeText(getApplicationContext(), meta.getAuthor(), Toast.LENGTH_LONG).show();
                if (adativeStream.isEmpty()) {
                    LogUtils.log("null ha");
                    return;
                }
                if (muxedStream.isEmpty()) {
                    LogUtils.log("null ha");
                    return;
                }
                FileLog.e("TAGGG", "Got a result with > SIZE <  " + muxedStream.size());
                String url = muxedStream.get(0).getUrl();

                //PlayVideo(url);

                binding.mPlayerView.setVisibility(View.VISIBLE);
                binding.mainMediaFrame.setVisibility(View.VISIBLE);

                String videoUrl = url;

                binding.mPlayerView.setTag(YOUTUBE_VIDEO.getVideoID());
                binding.mPlayerView.getControllerShowTimeoutMs();
                _manager = ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this);

                binding.mPlayerView.setPlayer(_manager.getPlayerView().getPlayer());
                binding.mPlayerView.UpdateObject(YOUTUBE_VIDEO, CategoryDetailActivity.this);
                ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this).playStream(videoUrl);

                if (isRedirect) {
                    if (_manager != null) {
                        _manager.stopPlayer(true);
                    }
                }

                FileLog.e("TAGGG", "Got a result with > SIZE <<  " + muxedStream.size() + " Redirect " + isRedirect);

            }
        }).useDefaultLogin().Extract(Constants.GET_YOUTUBE_URL + YOUTUBE_VIDEO.getYoutubeVideoID());*/

       /* new YoutubeStreamExtractor(new YoutubeStreamExtractor.ExtractorListner() {

            @Override
            public void onExtractionDone(List<YTMedia> adativeStream, final List<YTMedia> muxedStream, YoutubeMeta meta) {


                if (muxedStream.isEmpty()) {
                    FileLog.e("TAGGG", "okhttp result null");
                    return;
                }

                FileLog.e("TAGGG", "okhttp result YoutubeStreamExtractor onExtractionDone> " + YOUTUBE_VIDEO);
                playVideo(muxedStream, YOUTUBE_VIDEO);


            }


            @Override
            public void onExtractionGoesWrong(final ExtractorException e) {

                //Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                FileLog.e("TAGGG", "okhttp result YoutubeStreamExtractor onExtractionGoesWrong> " + e.getMessage(), e);

            }
        }).Extract(Constants.GET_YOUTUBE_URL + YOUTUBE_VIDEO.getYoutubeVideoID());*/


    }

   /* private void ExtractVideo(final CategoryVideoModel.Video_List YOUTUBE_VIDEO) {

        FileLog.e("TAGGG", "Got a result with > onSuccess <  " + YOUTUBE_VIDEO.getYoutubeVideoID());

        Toast.makeText(getApplicationContext(), "Please wait", Toast.LENGTH_LONG).show();

        new YoutubeStreamExtractor(new YoutubeStreamExtractor.ExtractorListner() {


            @Override
            public void onExtractionGoesWrong(final ExtractorException e) {

              //  Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();


                FileLog.e("TAGGG", "Got a result with > Error <  " + e.getMessage() ,e);
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();


                String url = "https://www.radiantmediaplayer.com/media/bbb-360p.mp4";

                //PlayVideo(url);

                binding.mPlayerView.setVisibility(View.VISIBLE);
                binding.mainMediaFrame.setVisibility(View.VISIBLE);

                String videoUrl = url;

                binding.mPlayerView.setTag(YOUTUBE_VIDEO.getVideoID());
                binding.mPlayerView.getControllerShowTimeoutMs();
                _manager = ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this);

                binding.mPlayerView.setPlayer(_manager.getPlayerView().getPlayer());
                binding.mPlayerView.UpdateObject(YOUTUBE_VIDEO, CategoryDetailActivity.this);
                ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this).playStream(videoUrl);

                if (isRedirect) {
                    if (_manager != null) {
                        _manager.stopPlayer(true);
                    }
                }


            }

            @Override
            public void onExtractionDone(List<com.paul.ttf.Extract.model.YTMedia> adativeStream, List<com.paul.ttf.Extract.model.YTMedia> muxedStream, List<YTSubtitles> subList, com.paul.ttf.Extract.model.YoutubeMeta meta) {

                //url to get subtitle
                //String subUrl=subtitles.get(0).getBaseUrl();


                urls_li.clear();
                for (YTMedia c : muxedStream) {

                    urls_li.add(c.getUrl());
                    // adapter.notifyDataSetChanged();
                }
                for (YTMedia media : adativeStream) {

                    urls_li.add(media.getUrl());
                    // adapter.notifyDataSetChanged();
                }
                //Toast.makeText(getApplicationContext(), meta.getTitle(), Toast.LENGTH_LONG).show();
                //Toast.makeText(getApplicationContext(), meta.getAuthor(), Toast.LENGTH_LONG).show();
                if (adativeStream.isEmpty()) {
                    LogUtils.log("null ha");
                    return;
                }
                if (muxedStream.isEmpty()) {
                    LogUtils.log("null ha");
                    return;
                }
                FileLog.e("TAGGG", "Got a result with > SIZE <  " + muxedStream.size());
                String url = muxedStream.get(0).getUrl();

                //PlayVideo(url);

                binding.mPlayerView.setVisibility(View.VISIBLE);
                binding.mainMediaFrame.setVisibility(View.VISIBLE);

                String videoUrl = url;

                binding.mPlayerView.setTag(YOUTUBE_VIDEO.getVideoID());
                binding.mPlayerView.getControllerShowTimeoutMs();
                _manager = ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this);

                binding.mPlayerView.setPlayer(_manager.getPlayerView().getPlayer());
                binding.mPlayerView.UpdateObject(YOUTUBE_VIDEO, CategoryDetailActivity.this);
                ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this).playStream(videoUrl);

                if (isRedirect) {
                    if (_manager != null) {
                        _manager.stopPlayer(true);
                    }
                }

                FileLog.e("TAGGG", "Got a result with > SIZE <<  " + muxedStream.size() + " Redirect " + isRedirect);

            }
        }).useDefaultLogin().Extract(Constants.GET_YOUTUBE_URL + YOUTUBE_VIDEO.getYoutubeVideoID());

       *//* new YoutubeStreamExtractor(new YoutubeStreamExtractor.ExtractorListner() {

            @Override
            public void onExtractionDone(List<YTMedia> adativeStream, final List<YTMedia> muxedStream, YoutubeMeta meta) {


                if (muxedStream.isEmpty()) {
                    FileLog.e("TAGGG", "okhttp result null");
                    return;
                }

                FileLog.e("TAGGG", "okhttp result YoutubeStreamExtractor onExtractionDone> " + YOUTUBE_VIDEO);
                playVideo(muxedStream, YOUTUBE_VIDEO);


            }


            @Override
            public void onExtractionGoesWrong(final ExtractorException e) {

                //Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                FileLog.e("TAGGG", "okhttp result YoutubeStreamExtractor onExtractionGoesWrong> " + e.getMessage(), e);

            }
        }).Extract(Constants.GET_YOUTUBE_URL + YOUTUBE_VIDEO.getYoutubeVideoID());*//*


    }*/

    private void playVideo(List<YTMedia> muxedStream, CategoryVideoModel.Video_List YOUTUBE_VIDEO) {
        for (int i = 0; i < muxedStream.size(); i++) {
            try {
                Log.d("TAGGGG", "Got a result with the best videoUrl:  " + muxedStream.get(i).getUrl());
                if (muxedStream.get(i).getUrl() != null && !muxedStream.get(i).getUrl().equals("")) {

                    binding.mPlayerView.setVisibility(View.VISIBLE);
                    binding.mainMediaFrame.setVisibility(View.VISIBLE);

                    String videoUrl = muxedStream.get(i).getUrl();

                    binding.mPlayerView.setTag(YOUTUBE_VIDEO.getVideoID());
                    binding.mPlayerView.getControllerShowTimeoutMs();
                    _manager = ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this);

                    binding.mPlayerView.setPlayer(_manager.getPlayerView().getPlayer());
                    binding.mPlayerView.UpdateObject(YOUTUBE_VIDEO, this);
                    ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this).playStream(videoUrl);
                    Log.d("TAGGGG", "Got a result with the best DONE videoUrl <  " + muxedStream.get(i).getUrl());
                    break;
                }

            } catch (Exception ex) {
                Log.d("TAGGGG", "Got a result with the best error:  " + ex.getMessage(), ex);
            }
        }
    }

    /*private void playVideo(YouTubeExtraction result, CategoryVideoModel.Video_List YOUTUBE_VIDEO) {
        for (int i = 0; i < result.getVideoStreams().size(); i++) {
            try {
                Log.d("TAGGGG", "Got a result with the best videoUrl:  " + result.getVideoStreams().get(i).getUrl());
                if (result.getVideoStreams().get(i).getUrl() != null && !result.getVideoStreams().get(i).getUrl().equals("")) {
                    String videoUrl = result.getVideoStreams().get(i).getUrl();

                    binding.mPlayerView.setTag(YOUTUBE_VIDEO.getVideoID());
                    binding.mPlayerView.getControllerShowTimeoutMs();
                    _manager = ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this);
                    binding.mPlayerView.setPlayer(_manager.getPlayerView().getPlayer());
                    binding.mPlayerView.UpdateObject(YOUTUBE_VIDEO);
                    ExoPlayerManager.getSharedInstance(CategoryDetailActivity.this).playStream(videoUrl);

                    break;
                }

            } catch (Exception ex) {
                Log.d("TAGGGG", "Got a result with the best error:  " + ex.getMessage(), ex);
            }
        }
    }*/

    private long getSecondsFormat(String time) {

        long totalSeconds = 0;
        try {
            String[] sampleTime = time.split(":");
            int int_hour = Integer.parseInt(sampleTime[0]);
            int int_minute = Integer.parseInt(sampleTime[1]);
            int int_seconds = Integer.parseInt(sampleTime[2]);
            long secondInHour = TimeUnit.HOURS.toSeconds(int_hour);
            long secondInMinute = TimeUnit.MINUTES.toSeconds(int_minute);
            totalSeconds = (secondInHour + secondInMinute + int_seconds);
            FileLog.e("TAGGG", "Check Total Seconds > " + secondInHour + " MIN " + secondInMinute + " SEC " + int_seconds + " TOT " + (secondInHour + secondInMinute + int_seconds));

        } catch (Exception ex) {
            FileLog.e("TAGGG", "Check Total Seconds error > " + ex.getMessage(), ex);
        }


        return totalSeconds;
    }

    private void releasePlayer() {
        FileLog.e("TAGGG", "Check vlogs > onDestroy() " + (_manager != null));
        if (_manager != null) {
            _manager.getReleaseObject();
        }
    }

    @Override
    public void onSelectOptionItem(CategoryVideoModel.Video_List position) {

    }

    @Override
    public void onHideDialog() {
        binding.mPlayerView.HideDialog();
    }

    @Override
    public void onExitActivity() {
        Intent i = new Intent(CategoryDetailActivity.this, SplashActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtra("subscribe", "yes");
        startActivity(i);
        finish();
    }

    @Override
    public void Setfullscreen(boolean isFull) {
        if (isFull) {
            openFullscreenDialog();
        } else {
            closeFullscreenDialog();
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checking the orientation of the screen
        FileLog.e("TAGGGG", "onConfigurationChanged Change > " + (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE));

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //First Hide other objects (listview or recyclerview), better hide them using Gone.
            /*FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) binding.mPlayerView.getLayoutParams();
            params.width = params.MATCH_PARENT;
            params.height = params.MATCH_PARENT;
            binding.mPlayerView.setLayoutParams(params);*/
            if (_manager != null && _manager.isPlayerPlaying()) {
                openFullscreenDialog();
            }


        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            //unhide your objects here.
            /*FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) binding.mPlayerView.getLayoutParams();
            params.width = params.MATCH_PARENT;
            params.height = 600;
            binding.mPlayerView.setLayoutParams(params);*/

            closeFullscreenDialog();
        }
    }

    private void openFullscreenDialog() {
        binding.mPlayerView.UpdateIcon(false);
        ((ViewGroup) binding.mPlayerView.getParent()).removeView(binding.mPlayerView);
        mFullScreenDialog.addContentView(binding.mPlayerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        //mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_fullscreen_skrink));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
    }

    private void closeFullscreenDialog() {
        binding.mPlayerView.UpdateIcon(true);
        ((ViewGroup) binding.mPlayerView.getParent()).removeView(binding.mPlayerView);
        ((FrameLayout) findViewById(R.id.main_media_frame)).addView(binding.mPlayerView);
        mExoPlayerFullscreen = false;
        mFullScreenDialog.dismiss();
        //mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(CategoryDetailActivity.this, R.drawable.ic_fullscreen_expand));
    }

    @Override
    public boolean onTouch(View v, MotionEvent motionEvent) {

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // touch down code
                showDatePicker();

                break;

            case MotionEvent.ACTION_MOVE:
                // touch move code
                break;

            case MotionEvent.ACTION_UP:
                // touch up code
                break;
        }

        return true;
    }

    public void showDatePicker() {

        mDialog.show();
    }

    private void GetUserSurvaydata() {


        APIInterface gerritAPI;
        if (!_shared_prefrance.getToken().equals("")) {
            gerritAPI = APIClient.getClientHeader(_shared_prefrance.getToken()).create(APIInterface.class);
        } else {
            gerritAPI = APIClient.getClientHeader(Constants.Guest_Token).create(APIInterface.class);
        }

        SharedPreferencesUtility _sharePrefrance = new SharedPreferencesUtility(ApplicationClass.applicationContext);

        UsereviewRequestModel _model = new UsereviewRequestModel();
        _model.setCategoryID(Categoryid);
        _model.setMemberID(_sharePrefrance.getMemberid());
        _model.setVideoID(_video_obj.getVideoID());
        _model.setFromDate(From_date);
        _model.setToDate(To_date);


        Observable<UsereviewResponseModel> profileResponse = gerritAPI.GetUserSurvay(_model);
        doProgressStart();
        profileResponse.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<UsereviewResponseModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(final UsereviewResponseModel categoryListModel) {


                if (categoryListModel.getCode() == 200) {
                    FileLog.e("TAGGG", "Number converter String  here onNext>> " + categoryListModel.getLstRecords().size());
                    if (categoryListModel.getLstRecords().size() != 0) {
                        //setData(categoryListModel);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                LoadBarChart(categoryListModel);
                                //binding.chart.setExtraOffsets(10, 10, 10, 10);
                                //xAxisFormatter.DayAxisValueUpdate(categoryListModel);
                                binding.chart.invalidate();
                            }
                        });


                    } else {

                        //setData(categoryListModel);
                        //generateLineData(categoryListModel);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                LoadBarChart(categoryListModel);
                                // binding.chart.setExtraOffsets(10, 10, 10, 10);
                                //xAxisFormatter.DayAxisValueUpdate(categoryListModel);
                                binding.chart.invalidate();
                            }
                        });


                        Snackbar snackbar = Snackbar
                                .make(binding.coordinateParent, getResources().getString(R.string.txt_no_history), Snackbar.LENGTH_LONG);

                        snackbar.setActionTextColor(Color.RED);
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    }

                } else if (categoryListModel.getCode() == 401) {

                    ShowAuthError(categoryListModel.getMessage(), getString(R.string.error_auth));

                } else {
                    Snackbar snackbar = Snackbar
                            .make(binding.coordinateParent, categoryListModel.getMessage(), Snackbar.LENGTH_LONG);

                    snackbar.setActionTextColor(Color.RED);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                }

            }

            @Override
            public void onError(Throwable e) {
                DismissProgress();
            }

            @Override
            public void onComplete() {
                DismissProgress();
                FileLog.e("TAGGG", "Number converter String  here onComplete>> ");
            }
        });

    }

    private void FillVideoSpinner(ArrayList<CategoryVideoModel.Video_List> videoListModel) {

        CustomVideoAdapter adapter = new CustomVideoAdapter(CategoryDetailActivity.this,
                R.layout.rowitem_layout, R.id.txt_label, videoListModel);
        binding.spinnerVideo.setAdapter(adapter);
        binding.spinnerVideo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                _video_obj = (AllCategoryVideoModel.List.VideoList) view.getTag();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    //private void setData(int count, float range, UsereviewResponseModel categoryListModel) {
    /*private void setData(UsereviewResponseModel categoryListModel) {


        ArrayList<BarEntry> values = new ArrayList<>();

        for (int i = 0; i < categoryListModel.getLstRecords().size(); i++) {
            FileLog.e("TAGGG", "sample test count > " + categoryListModel.getLstRecords().get(i).getPopupValue());
            float value = categoryListModel.getLstRecords().get(i).getPopupValue();
            values.add(new BarEntry(i, categoryListModel.getLstRecords().get(i).getPopupValue(), getResources().getDrawable(R.drawable.star)));
        }


        BarDataSet set1;

        if (binding.chart.getData() != null &&
                binding.chart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) binding.chart.getData().getDataSetByIndex(0);


            set1.setValues(values);
            binding.chart.getData().notifyDataChanged();
            binding.chart.notifyDataSetChanged();

        } else {
            set1 = new BarDataSet(values, "");

            set1.setDrawIcons(false);

//            set1.setColors(ColorTemplate.MATERIAL_COLORS);

            *//*int startColor = ContextCompat.getColor(this, android.R.color.holo_blue_dark);
            int endColor = ContextCompat.getColor(this, android.R.color.holo_blue_bright);
            set1.setGradientColor(startColor, endColor);*//*

            int startColor1 = ContextCompat.getColor(this, android.R.color.black);


            List<GradientColor> gradientColors = new ArrayList<>();
            gradientColors.add(new GradientColor(startColor1, startColor1));
            gradientColors.add(new GradientColor(startColor1, startColor1));
            gradientColors.add(new GradientColor(startColor1, startColor1));
            gradientColors.add(new GradientColor(startColor1, startColor1));
            gradientColors.add(new GradientColor(startColor1, startColor1));

            set1.setGradientColors(gradientColors);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            //data.setValueTypeface(tfLight);
            data.setBarWidth(0.9f);

            ArrayList<Entry> entries = new ArrayList<>();


            for (int index = 0; index < categoryListModel.getLstRecords().size(); index++) {
                entries.add(new Entry(index, categoryListModel.getLstRecords().get(index).getPopupValue()));
            }

            LineDataSet set = new LineDataSet(entries, "Line DataSet");
            set.setColor(Color.rgb(240, 238, 70));
            set.setLineWidth(2.5f);
            set.setCircleColor(Color.rgb(240, 238, 70));
            set.setCircleRadius(5f);
            set.setFillColor(Color.rgb(240, 238, 70));
            set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set.setDrawValues(true);
            set.setValueTextSize(10f);
            set.setValueTextColor(Color.rgb(240, 238, 70));

            set.setAxisDependency(YAxis.AxisDependency.LEFT);


            binding.chart.setData(set);
        }
    }*/

    private LineData generateLineData(UsereviewResponseModel categoryListModel) {

        LineData d = new LineData();

        ArrayList<Entry> entries = new ArrayList<>();

        for (int index = 0; index < categoryListModel.getLstRecords().size(); index++)
            entries.add(new Entry(index, categoryListModel.getLstRecords().get(index).getPopupValue()));

        LineDataSet set = new LineDataSet(entries, "Line DataSet");
        set.setColor(getResources().getColor(R.color.black));
        set.setLineWidth(2.5f);
        set.setCircleColor(getResources().getColor(R.color.black));
        set.setCircleRadius(5f);
        set.setFillColor(getResources().getColor(R.color.black));
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setDrawValues(true);
        set.setValueTextSize(10f);
        set.setValueTextColor(getResources().getColor(R.color.black));

        set.setDrawFilled(true);
        if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fill_chart);
            set.setFillDrawable(drawable);
        } else {
            set.setFillColor(R.color.black);
        }


        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        d.addDataSet(set);

        return d;
    }

    private boolean IsValid() {
        boolean isvalid = true;
        if (binding.editFromdate.getText().toString().trim().equals("")) {

            Snackbar snackbar = Snackbar
                    .make(binding.coordinateParent, getString(R.string.text_from_hint), Snackbar.LENGTH_LONG);

            snackbar.setActionTextColor(Color.RED);
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();

            isvalid = false;
        }

        return isvalid;
    }

    public boolean ShowAuthError(String msg_title, String msg_detail) {


        TextView title = new TextView(this);
        title.setText(msg_title);
        title.setPadding(15, 15, 15, 10);
        title.setGravity(Gravity.LEFT);
// title.setTextColor(getResources().getColor(R.color.greenBG));
        title.setTextSize(18);

        TextView msg = new TextView(this);
        msg.setText(msg_detail);
        msg.setPadding(15, 10, 15, 10);
        msg.setGravity(Gravity.LEFT);
        msg.setTextSize(16);

        DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) { //Delete for me

                    SharedPreferencesUtility _sharePreference = new SharedPreferencesUtility(CategoryDetailActivity.this);
                    _sharePreference.ClearPrefrance();

                    Intent intent = new Intent(CategoryDetailActivity.this, SplashActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();

                    dialog.dismiss();
                }

            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCustomTitle(title);
        builder.setView(msg);
        //builder.setCancelable(true);
        builder.setPositiveButton(getString(R.string.txt_logout), onClick);
        //builder.setNegativeButton(getString(R.string.txt_delete_cancel), onClick);


        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        return true;
    }

    @Override
    public void PauseVideoHere() {
        try {

            FileLog.e("TAGGG", "Got a result with > SIZE <PAUSE  " + (_manager != null));
            if (_manager != null) {
                _manager.stopPlayer(true);
            }

        } catch (Exception ex) {
        }
    }

    @Override
    public void onShowView() {
        binding.transparentFrame.setVisibility(View.VISIBLE);
        binding.transparentFrameSecond.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideView() {
        binding.transparentFrame.setVisibility(View.GONE);
        binding.transparentFrameSecond.setVisibility(View.GONE);
    }



}
