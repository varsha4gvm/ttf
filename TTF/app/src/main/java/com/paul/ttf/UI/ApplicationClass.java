package com.paul.ttf.UI;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

import androidx.core.app.NotificationManagerCompat;

import com.paul.ttf.CustomControl.TypeFactory;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class ApplicationClass extends Application {

    public static volatile Context applicationContext;
    public static NotificationManagerCompat mNotificationManagerCompat;


    private static ApplicationClass mInstance;
    private TypeFactory mFontFactory;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        applicationContext = getApplicationContext();
        mNotificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());
        mInstance = this;
    }

    public static synchronized ApplicationClass getApp() {
        return mInstance;
    }

    public Typeface getTypeFace(int type) {
        if (mFontFactory == null)
            mFontFactory = new TypeFactory(this);

        switch (type) {
            case Constants.REGULAR:
                return mFontFactory.getRegular();

            case Constants.BOLD:
                return mFontFactory.getBold();

            case Constants.HEAVY:
                return mFontFactory.getHeavy();

            case Constants.SEMIBOLD:
                return mFontFactory.getSemibold();

            default:
                return mFontFactory.getRegular();
        }
    }

    public interface Constants {
        int REGULAR = 1,
                BOLD = 2,
                SEMIBOLD = 3,
                HEAVY = 4;
    }

    public static NotificationManagerCompat getmNotificationManagerCompat() {
        return mNotificationManagerCompat;
    }

  /*  static ApiClient getApiClient(Context context) {
        if (sApiClient == null) {
            sApiClient = new RestAdapter.Builder()
                    .setEndpoint(Settings.getEnvironmentUrl(context))
                    .setRequestInterceptor(new ApiClientRequestInterceptor())
                    .build()
                    .create(ApiClient.class);
        }

        return sApiClient;
    }*/

}
