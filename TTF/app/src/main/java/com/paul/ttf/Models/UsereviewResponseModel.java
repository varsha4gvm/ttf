package com.paul.ttf.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UsereviewResponseModel {

    @SerializedName("code")
    public Integer code;

    @SerializedName("message")
    public String message;

    @SerializedName("lstRecords")
    public ArrayList<lstRecords> lstRecords;


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<UsereviewResponseModel.lstRecords> getLstRecords() {
        return lstRecords;
    }

    public void setLstRecords(ArrayList<UsereviewResponseModel.lstRecords> lstRecords) {
        this.lstRecords = lstRecords;
    }

    public class lstRecords {

        @SerializedName("PopupID")
        public Integer PopupID;

        @SerializedName("PopupValue")
        public float PopupValue;

        @SerializedName("Date")
        public String Date;

        public Integer getPopupID() {
            return PopupID;
        }

        public void setPopupID(Integer popupID) {
            PopupID = popupID;
        }

        public float getPopupValue() {
            return PopupValue;
        }

        public void setPopupValue(float popupValue) {
            PopupValue = popupValue;
        }

        public String getDate() {
            return Date;
        }

        public void setDate(String date) {
            Date = date;
        }
    }


}