package com.paul.ttf.Utility;

public class Constants {

    public static String BASE_URL = "http://ttf.gvmtechnologies.com/";

    public static String Guest_Token = "guest-token-443d605b-abd8-4d59-a747-36b26ea9625e";
    public static String GET_CATEGORY_URL = "api/CategoryList/CategoryList";
    public static String GET_CATEGORY_VIDEO_URL = "api/VideoList/VideoList";
    public static String GET_ALL_CATEGORY_VIDEO_URL = "api/getList/Getcategoryvideolist";
    public static String GET_CHECK_SUBSCRIPTION_URL = "api/check/CheckSubscription";



    public static String GET_YOUTUBE_URL = "https://youtu.be/";
    public static String GET_SIGNUP_URL = "https://ttfkickboxing.gymmasteronline.com/portal/signup/details/0263ff0fbd9672b5ca7fa1d310700738?companyid=2";
    public static int LIMIT=59;
}
